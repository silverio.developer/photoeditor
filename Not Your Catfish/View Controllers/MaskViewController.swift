//
//  MaskViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/08/31.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit
import CropViewController
import Fritz
import YUCIHighPassSkinSmoothing
import ColorSlider
import NVActivityIndicatorView
import Lottie

class MaskViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    let imageNames = ["1","2", "3", "4","5","6", "7", "8","9","10", "11", "12","13","14", "15", "16","17","18", "19", "20","21","22", "23", "24","25","26", "27", "28","29","30", "31", "32","33","34", "35", "36","37","38", "39", "40","41","42", "43", "44","45","46", "47", "48","49","50", "51", "52","automn"]
    
    let modeImages = ["woman", "female-hair", "hula-hoop", "eraser", "layers"]
    let modeTitles = ["Auto Mask", "Background", "mask", "Erase", "Double Expo"]
    
    let amountSlider: UISlider = {
        
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 1
        slider.value = 0.7
        
        return slider
        
    }()
    
    var topView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var topShadowLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .lightGray
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var middleView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        
        
        return vw
        
    }()
    
    var bottomView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        
        return vw
        
    }()
    
    var imageBackground: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        //img.backgroundColor = .yellow
        img.image = UIImage(named: "ehChick")
        img.contentMode = .scaleToFill
        //img.image = UIImage(named: "reach")
        img.layer.zPosition = -2
        
        return img
        
    }()
    
    var imageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "ehChick")
        img.contentMode = .scaleAspectFit
        img.layer.zPosition = -1
        
        return img
        
    }()
    
    var maskImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "ehChick")
        img.contentMode = .scaleAspectFit
        img.layer.zPosition = -2
        
        return img
        
    }()
    
    var previousImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "backward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    lazy var previousBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(previousF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var forwardBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    var forwardImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "foward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    var nycImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "maskLogo")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var menuCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCell.self, forCellWithReuseIdentifier: "MenuId")
        cv.backgroundColor = .clear
        
        return cv
        
    }()
    
    var exposureImagesCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(ExpoCell.self, forCellWithReuseIdentifier: "ExpoId")
        cv.backgroundColor = .white
        cv.alpha = 0
        
        return cv
        
    }()
    
    var compareView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.cornerRadius = 8
        vw.layer.borderColor = UIColor.black.cgColor
        vw.layer.borderWidth = 2
        
        
        return vw
        
    }()
    
    var compareImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "compare")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    let compareLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .white
        lbl.text = "Long Press To Compare"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 15)
        lbl.layer.masksToBounds = true
        lbl.layer.cornerRadius = 8
        lbl.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        lbl.layer.zPosition = -1
        
        return lbl
        
    }()
    
    lazy var saveCurrentImage: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveCurrentImageF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var cancelTranform: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelTransoformF), for: .touchUpInside)
        
        return btn
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func addGestureRecognizer(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        
        longPress.delegate = self
        
        imageView.addGestureRecognizer(longPress)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var heightConstraint: NSLayoutConstraint!
    
    var cropViewController: CropViewController!
    
    lazy var balloonPickerView: BalloonPickerView = {
        
        let bPV = BalloonPickerView()
        bPV.translatesAutoresizingMaskIntoConstraints = false
        bPV.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        bPV.value = 70
        bPV.addTarget(self, action: #selector(balloonChanged), for: .valueChanged)
        
        
        return bPV
        
    }()
    
    var sliderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        return vw
        
    }()
    
    lazy var cancelSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var saveSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let sliderLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        lbl.text = "Erase"
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir-Light", size: 18)
        
        return lbl
        
    }()
    
    var loaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 18
        vw.layer.masksToBounds = true
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoader: NVActivityIndicatorView = {
        
        let loader = NVActivityIndicatorView(frame: .zero)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.type = .ballTrianglePath
        loader.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.color = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.layer.zPosition = 5
        
        return loader
        
    }()
    
    var lightenSizeBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        vw.alpha = 0
        vw.layer.cornerRadius = 12
        vw.layer.masksToBounds = true
        
        return vw
        
    }()
    
    var lightenSizeWheel: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .clear
        vw.layer.borderColor = UIColor.white.cgColor
        vw.layer.borderWidth = 2
        vw.layer.cornerRadius = 20
        vw.layer.masksToBounds = true
        
        
        return vw
        
    }()
    
    var lightenSizeWidth: NSLayoutConstraint!
    var lightenSizeHeight: NSLayoutConstraint!
    
    var topConstraint: NSLayoutConstraint!
    
    var maskHeightConstraint: NSLayoutConstraint!
    
    func setup(){
        
        var widthSize = view.frame.width - 16
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                widthSize = view.frame.width / 1.30
                
            case 1334:
                print("iPhone 6/6S/7/8")
                widthSize = view.frame.width / 1.25
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                widthSize = view.frame.width / 1.25
                
            case 2436:
                print("iPhone X, XS")
                
            case 2688:
                print("iPhone XS Max")
                
            case 1792:
                print("iPhone XR")
                
            default:
                print("Unknown")
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            
            widthSize = view.frame.width / 1.9
            
        }
        
        addGestureRecognizer()
        
        
        view.backgroundColor = UIColor.init(red: 212/255, green: 213/255, blue: 214/255, alpha: 1)
        
        view.addSubview(topView)
        
        view.addSubview(imageBackground)
        view.addSubview(imageView)
        view.addSubview(maskImageView)
        view.addSubview(previousBtn)
        view.addSubview(forwardBtn)
        view.addSubview(previousImage)
        view.addSubview(forwardImage)
        
        view.addSubview(loaderBackground)
        view.addSubview(lightenSizeBackground)
        lightenSizeBackground.addSubview(lightenSizeWheel)
        
        
        
        menuCV.delegate = self
        menuCV.dataSource = self
        
        exposureImagesCV.delegate = self
        exposureImagesCV.dataSource = self
        
        //view.addSubview(compareView)
        //compareView.addSubview(compareImageView)
        view.addSubview(compareLbl)
        
        view.addSubview(bottomView)
        bottomView.addSubview(menuCV)
        
        view.addSubview(sliderBackground)
        sliderBackground.addSubview(balloonPickerView)
        sliderBackground.addSubview(cancelSlider)
        sliderBackground.addSubview(saveSlider)
        sliderBackground.addSubview(sliderLbl)
        
        topView.addSubview(nycImageView)
        
        topView.addSubview(saveCurrentImage)
        topView.addSubview(cancelTranform)
        
        view.addSubview(loadingLoaderBackground)
        loadingLoaderBackground.addSubview(loadingLoader)
        
        view.addSubview(exposureImagesCV)
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        sliderBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        sliderBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topConstraint = sliderBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 105)
        topConstraint.isActive = true
        sliderBackground.heightAnchor.constraint(equalToConstant: 105).isActive = true
        
        saveSlider.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        saveSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        cancelSlider.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 15).isActive = true
        cancelSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        sliderLbl.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -20).isActive = true
        sliderLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        sliderLbl.widthAnchor.constraint(equalToConstant: 155).isActive = true
        sliderLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        balloonPickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        balloonPickerView.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -55).isActive = true
        balloonPickerView.widthAnchor.constraint(equalToConstant: 325).isActive = true
        balloonPickerView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        let balloonView = BalloonView()
        balloonView.image = #imageLiteral(resourceName: "balloon")
        balloonPickerView.baloonView = balloonView
        
        bottomView.bottomAnchor.constraint(equalTo: sliderBackground.topAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: view.frame.width / 5 + 25).isActive = true
        
        imageBackground.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        imageBackground.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -2).isActive = true
        imageBackground.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        imageBackground.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -2).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageBackground.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBackground.addSubview(blurEffectView)
        
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        
        maskImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        maskHeightConstraint = maskImageView.heightAnchor.constraint(equalToConstant: 0)
        maskHeightConstraint.isActive = true
        maskImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        maskImageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        maskImageView.alpha = 0
        
        //imageView.frame.size.width = view.frame.size.width - 16
        //imageView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -8).isActive = true
        
        
        
        forwardBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 35).isActive = true
        forwardBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        previousBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -35).isActive = true
        previousBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        forwardImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.centerXAnchor.constraint(equalTo: forwardBtn.centerXAnchor).isActive = true
        forwardImage.centerYAnchor.constraint(equalTo: forwardBtn.centerYAnchor).isActive = true
        
        previousImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.centerXAnchor.constraint(equalTo: previousBtn.centerXAnchor).isActive = true
        previousImage.centerYAnchor.constraint(equalTo: previousBtn.centerYAnchor).isActive = true
        
        menuCV.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 15).isActive = true
        menuCV.heightAnchor.constraint(equalToConstant: view.frame.width / 5).isActive = true
        menuCV.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        menuCV.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
        
        /*compareView.heightAnchor.constraint(equalToConstant: 60).isActive = true
         compareView.widthAnchor.constraint(equalToConstant:60).isActive = true
         compareView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 5).isActive = true
         compareView.topAnchor.constraint(equalTo: menuCV.bottomAnchor, constant: 5).isActive = true*/
        
        compareLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        compareLbl.widthAnchor.constraint(equalToConstant: 195).isActive = true
        compareLbl.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -25).isActive = true
        compareLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
        nycImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 10).isActive = true
        nycImageView.widthAnchor.constraint(equalToConstant: view.frame.width - 55).isActive = true
        nycImageView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 40).isActive = true
        nycImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        saveCurrentImage.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -15).isActive = true
        saveCurrentImage.heightAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.widthAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        cancelTranform.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 15).isActive = true
        cancelTranform.heightAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.widthAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        loaderBackground.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        loaderBackground.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        loaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoaderBackground.widthAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.heightAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoader.widthAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.heightAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        lightenSizeBackground.heightAnchor.constraint(equalToConstant: 125).isActive = true
        lightenSizeBackground.widthAnchor.constraint(equalToConstant: 125).isActive = true
        lightenSizeBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lightenSizeBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        lightenSizeHeight = lightenSizeWheel.heightAnchor.constraint(equalToConstant: 40)
        lightenSizeHeight.isActive = true
        lightenSizeWidth = lightenSizeWheel.widthAnchor.constraint(equalToConstant: 40)
        lightenSizeWidth.isActive = true
        lightenSizeWheel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lightenSizeWheel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        exposureImagesCV.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 8).isActive = true
        exposureImagesCV.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -8).isActive = true
        exposureImagesCV.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8).isActive = true
        exposureImagesCV.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8).isActive = true
        
        /*compareImageView.leftAnchor.constraint(equalTo: compareView.leftAnchor).isActive = true
         compareImageView.rightAnchor.constraint(equalTo: compareView.rightAnchor).isActive = true
         compareImageView.topAnchor.constraint(equalTo: compareView.topAnchor).isActive = true
         compareImageView.bottomAnchor.constraint(equalTo: compareLbl.topAnchor).isActive = true*/
        
        //menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        
        heightConstraint.isActive = false
        
        maskHeightConstraint.isActive = false
        
        let ratio = imageView.image!.size.width / imageView.image!.size.height
        
        print(imageView.frame.width)
        let newHeight = widthSize / ratio
        heightConstraint.constant = newHeight
        heightConstraint.isActive = true
        
        maskHeightConstraint.constant = newHeight
        maskHeightConstraint.isActive = true
        
        view.layoutIfNeeded()
     
        
    }
    
    func saveCurrentImageStateF(){
        
        let newImage = self.imageView.image
        let imageData: NSData = newImage!.pngData()! as NSData
        
        //
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            let newImageNumber = imageNumber + 1
            
            UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
            UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
            UserDefaults.standard.set(newImageNumber, forKey: "maxImageNumber")
            
            
        }
    }
    
    var lineWidth = 20.0
    var onMask = false
    
    @objc func balloonChanged(){
        
        //processImage()
        if eraseImage == true || onMask == true {
            
            lightenSizeBackground.alpha = 0.7
            
            lightenSizeHeight.isActive = false
            lightenSizeWidth.isActive = false
            
            lightenSizeHeight.constant = CGFloat(balloonPickerView.value)
            lightenSizeWidth.constant = CGFloat(balloonPickerView.value)
            lightenSizeWheel.layer.cornerRadius = CGFloat(balloonPickerView.value / 2)
            
            
            lightenSizeHeight.isActive = true
            lightenSizeWidth.isActive = true
            
            //  lightenSizeWheel.frame.origin = CGPoint(x: lightenSizeBackground.frame.midX, y: lightenSizeBackground.frame.midY)
            lineWidth = balloonPickerView.value / 2
            
            perform(#selector(removeLightenSizeBackground), with: nil, afterDelay: 0.7)
            
        }
    }
    
    @objc func removeLightenSizeBackground(){
        
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.lightenSizeBackground.alpha = 0
            
        }
        
    }
    
    @objc func previousF(){
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            if imageNumber != 1 {
                
                let newImageNumber = imageNumber - 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "image\(newImageNumber)") as? NSData {
                    
                    UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                    
                    imageView.image = UIImage(data: imageData as Data)
                    
                }
                
            }
            
        }
        
    }
    
    @objc func forwardF(){
        
        print("ayt")
        
        if let max = UserDefaults.standard.object(forKey: "maxImageNumber") as? Int {
            
            print("maxxed:")
            print(max)
            
            if let current = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                print("currented:")
                print(current)
                
                if max >= current + 1 {
                    
                    print("through")
                    
                    let newCurrent = current + 1
                    
                    UserDefaults.standard.set(newCurrent, forKey: "currentImageNumber")
                    
                    if let imageData = UserDefaults.standard.object(forKey: "image\(newCurrent)") as? NSData {
                        
                        UserDefaults.standard.set(imageData, forKey: "image\(newCurrent)")
                        
                        imageView.image = UIImage(data: imageData as Data)
                        
                        //self.updateImageConstraints()
                        
                    }
                    
                    
                }
                
            }
            
        }
        
        
    }
    
    func presentCropper(){
        
        cropViewController = CropViewController(image: imageView.image!)
        cropViewController.delegate = self
        
        
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            
            self.imageView.image = image
            
        }
        
    }
    
    @objc func cancelTransoformF(){
        
        if let number = UserDefaults.standard.object(forKey: "firstOpenNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "currentImageNumber")
            
            if let data = UserDefaults.standard.object(forKey: "firstOpen") as? NSData {
                
                UserDefaults.standard.set(data, forKey: "image\(number)")
                
            }
            
        }
        
        dismiss(animated: true, completion: nil)
        
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Image saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    let imagePickerController = UIImagePickerController()
    
    var fromExposureCV = false
    
    @objc func callAlert(){
        
        let alert = UIAlertController(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            
            self.imagePickerController.delegate = self
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
            
            //self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            
            self.imagePickerController.delegate = self
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.sourceType = .camera
            self.present(self.imagePickerController, animated: true, completion: nil)
            
            //self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
            
        }
     
        let stock = UIAlertAction(title: "Stock", style: .default) { (action) in
            
             self.fromExposureCV = true
            
             self.exposureImagesCV.alpha = 1
            
            //self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
            self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
            
        }
        
        alert.addAction(photoLibrary)
        alert.addAction(camera)
        alert.addAction(stock)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func saveCurrentImageF(){
        
        //save current state image
        
        dismiss(animated: true, completion: nil)
        
        /*imagePickerController.delegate = self
         imagePickerController.allowsEditing = false
         
         imagePickerController.sourceType = .photoLibrary
         present(imagePickerController, animated: true, completion: nil)*/
        
    }
    
    var selectedExposureImage: UIImage!
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            
            return self.imagePickerControllerDidCancel(picker)
            
        }
        
        self.selectedExposureImage = image
        //peopleApi()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        picker.delegate = nil
    }
    
    var longPressed = false
    
    var currentImageDisplayed: UIImage!
    
    @objc func handleLongPress(){
        
        
        
        if longPressed == false {
            
            currentImageDisplayed = imageView.image
            
            if let originalImage = UserDefaults.standard.object(forKey: "image1") as? NSData {
                
                self.imageView.image = UIImage(data: originalImage as Data)
                
                //self.updateImageConstraints()
                
            }
            
            longPressed = true
            
            
        }else {
            
            imageView.image = currentImageDisplayed
            
            //self.updateImageConstraints()
            
            longPressed = false
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == menuCV {
            
            return 1
            
        }else {
            
            return 1
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == menuCV {
            
            return 5
            
        }else {
            
            return imageNames.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == menuCV {
            
            let cell = menuCV.dequeueReusableCell(withReuseIdentifier: "MenuId", for: indexPath) as! MenuCell
            
            cell.modeImage.image = UIImage(named: modeImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
            //cell.modeImage.tintColor = .black
            //cell.modeTitle.textColor = .black
            
            
            cell.modeTitle.text = modeTitles[indexPath.row]
            
            return cell
            
        }else {
            
            let cell = exposureImagesCV.dequeueReusableCell(withReuseIdentifier: "ExpoId", for: indexPath) as! ExpoCell
            
            cell.expoImage.image = UIImage(named: imageNames[indexPath.row])
            
            
            return cell
            
        }
        
       
        
    }
    
    var expo = false
    
    var removedBackground = false
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == menuCV {
            
            if indexPath.row == 0 {
                
                removeExposureCV()
                
                removeSlider()
                
                peopleApi()
                
                removedBackground = true
                
                expo = false
                eraseImage = false
                onMask = false
                maskImageView.alpha = 0
                
            }else if indexPath.row == 1 {
                
                UIView.animate(withDuration: 0.7) {
                    
                    self.callAlert()
                    //self.exposureImagesCV.alpha = 1
                    
                }
                
                removeSlider()
                
                if removedBackground == true {
                    
                    //merge
                    
                }else {
                    
                    //remove then merge
                    
                }
                //removeFaceF()
                
                expo = false
                eraseImage = false
                onMask = false
                maskImageView.alpha = 0
                
            }else if indexPath.row == 2 {
                
                blurImage()
                removeSlider()
                removeExposureCV()
                expo = false
                onMask = true
                eraseImage = false
                maskImageView.alpha = 1
                
            }else if indexPath.row == 3 {
                
                removeExposureCV()
                
                topConstraint.isActive = false
                topConstraint.constant = -2
                topConstraint.isActive = true
                onMask = false
                maskImageView.alpha = 0
                
                
                UIView.animate(withDuration: 0.7) {
                    
                    self.view.layoutIfNeeded()
                    
                }
                
                expo = false
                eraseImage = true
                
            }else if indexPath.row == 4 {
                
                //removeExposureCV()
                
                UIView.animate(withDuration: 0.7) {
                    
                    self.callAlert()
                    //self.exposureImagesCV.alpha = 1
                    
                }
                
                removeSlider()
                
                eraseImage = false
                onMask = false
                maskImageView.alpha = 0
                
                expo = true
                //check if background is removed, remove background, color monochrome into black(whatever it keeps) -- merge with white background, cimaskalpha to remove black, merge with selected image
                
                
            }
            
        }else {
            
            if expo == false {
                
                removeExposureCV()
                
                if fromExposureCV == true {
                    
                    mergeBackgroundImageF(imageName: imageNames[indexPath.row])
                    
                }else {
                    
                    mergeBackgroundImageFFromImage(exposureImage: selectedExposureImage)
                    
                }
                
                self.fromExposureCV = false
                
                
                
            }else {
                
                if fromExposureCV == true {
                    
                    applyBlackAndWhite(imageName: imageNames[indexPath.row])
                    
                }else {
                    
                    applyBlackAndWhiteFromImage(exposureImage: selectedExposureImage)
                    
                }
                
                self.fromExposureCV = false
                
                removeExposureCV()
                
            }
            
            //
            
        }
        
    }
    
    func removeExposureCV(){
        
        UIView.animate(withDuration: 0.7) {
            
            self.exposureImagesCV.alpha = 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == menuCV {
            
            let size = CGSize(width: view.frame.width / 5, height: view.frame.width / 5)
            
            return size
            
        }else {
            
            let size = CGSize(width: view.frame.width / 3.5, height: view.frame.width / 3.5)
            
            return size
            
        }
        
    }
    
    let peopleModel = FritzVisionPeopleSegmentationModelAccurate()
    
    func peopleApi(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let image = FritzVisionImage(image: imageView.image!)
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            
            
            guard let result = try? self.peopleModel.predict(image),
                let rotatedImage = image.rotate()
                else { return }
            
            //let background = UIImage(pixelBuffer: rotatedImage)
            
            let mask = result.buildSingleClassMask(
                forClass: FritzVisionPeopleClass.person,
                clippingScoresAbove: 0.7,
                zeroingScoresBelow: 0.25
            )
            
            //use mask in cimasktoalpha --> it changes black content into transparent content ??
            
            let clippedMaskImage = image.masked(with: mask!)
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                self!.imageView.image = clippedMaskImage
                
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0
                self!.view.isUserInteractionEnabled = true
                self!.saveCurrentImageStateF()
                
            }
        }
        
    }
    
    @objc func removeFaceF(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let image = FritzVisionImage(image: imageView.image!)
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                
                
                self!.view.isUserInteractionEnabled = true
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0.7
                
                
            }
        }
        
    }
    
    @objc func removeHairF(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let image = FritzVisionImage(image: imageView.image!)
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            image.metadata = FritzVisionImageMetadata()
            image.metadata?.orientation = .up
            
            guard let result = try? self.visionModel.predict(image),
                let mask = result.buildSingleClassMask(
                    forClass: FritzVisionHairClass.hair,
                    clippingScoresAbove: self.clippingScoresAbove,
                    zeroingScoresBelow: self.zeroingScoresBelow,
                    resize: false,
                    color: self.color)
                else { return }
            
            let blended = image.blend(
                withMask: mask,
                blendKernel: self.blendMode,
                opacity: self.opacity
            )
            
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                
                self!.imageView.image = mask
                self!.view.isUserInteractionEnabled = true
                //self!.invertAndMerge(image: mask)
                //self!.loadingLoaderBackground.alpha = 0
                //self!.loadingLoader.stopAnimating()
                
                
            }
        }
        
    }
    
    var clippingScoresAbove: Double { return 0.7 }
    
    /// Values lower than this value will not appear in the mask.
    var zeroingScoresBelow: Double { return 0.3 }
    
    /// Controls the opacity the mask is applied to the base image.
    var opacity: CGFloat { return 0.7 }
    
    /// The method used to blend the hair mask with the underlying image.
    /// Soft light produces the best results in our tests, but check out
    /// .hue and .color for different effects.
    var blendMode: CIBlendKernel { return .softLight }
    
    /// Color of the mask.
    var color: UIColor { return .white }
    
    private lazy var visionModel = FritzVisionHairSegmentationModelAccurate()
    
    func changeHairColor(){
        
        let image = FritzVisionImage(image: imageView.image!)
        
        image.metadata = FritzVisionImageMetadata()
        image.metadata?.orientation = .up
        
        guard let result = try? visionModel.predict(image),
            let mask = result.buildSingleClassMask(
                forClass: FritzVisionHairClass.hair,
                clippingScoresAbove: clippingScoresAbove,
                zeroingScoresBelow: zeroingScoresBelow,
                resize: false,
                color: color)
            else { return }
        
        let blended = image.blend(
            withMask: mask,
            blendKernel: blendMode,
            opacity: opacity
        )
        
        DispatchQueue.main.async {
            self.imageView.image = mask
        }
        
    }
    
    @objc func invertAndMerge(image: UIImage){
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        
        //change image to black
        
        let filter: CIFilter = CIFilter(name: "CIColorMonochrome")! //CIPhotoEffectMono -> this makes sure you can still see some features of the image
        filter.setDefaults()
        filter.setValue(CoreImage.CIImage(image: image)!, forKey: kCIInputImageKey)
        filter.setValue(CIColor.white, forKey: kCIInputColorKey)
        //filter.setValue(UIColor.black.ciColor, forKey: kCIInputColorKey)
        //imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: originalImageExtent!)!)
        
        //2. merge
        //3. remove
        
        
        //invert image to white
        
       /* let invertFilter: CIFilter = CIFilter(name: "CIColorInvert")!
        invertFilter.setDefaults()
        invertFilter.setValue(CoreImage.CIImage(image: imageView.image!)!, forKey: kCIInputImageKey)
        imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(invertFilter.outputImage!, from: originalImageExtent!)!)*/
        
        //removes black and keeps white
        
        let alphaFilter: CIFilter = CIFilter(name: "CIMaskToAlpha")!
        alphaFilter.setDefaults()
        print(alphaFilter.inputKeys)
        alphaFilter.setValue(CoreImage.CIImage(image: imageView.image!)!, forKey: kCIInputImageKey)
        //imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(alphaFilter.outputImage!, from: originalImageExtent!)!)
        
        DispatchQueue.main.async {
            
            self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: originalImageExtent!)!)
            
            self.loadingLoaderBackground.alpha = 0
            self.loadingLoader.stopAnimating()
            self.loaderBackground.alpha = 0.7
            self.view.isUserInteractionEnabled = true
            
        }
        
        
        //merge
        
        /*let backgroundCIImage = CIImage(image: UIImage(named: "ehChick")!)
        
        
        /*let inputImage = imageView.transform(by: CGAffineTransform(scaleX: backgroundImage.extent.size.width / imageView.image.extent.size.with, y: backgroundImage.extent.size.height / imageView.image.extent.size.height))*/
        
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        var inputImage = sourceCIImageWithoutBackground
        
        
        inputImage = inputImage?.transformed(by: CGAffineTransform(scaleX: backgroundCIImage!.extent.size.width / sourceCIImageWithoutBackground!.extent.size.width, y: backgroundCIImage!.extent.size.height / sourceCIImageWithoutBackground!.extent.size.height))
        
        
        
        let compositor = CIFilter(name:"CISourceOverCompositing")
        compositor?.setValue(inputImage, forKey: kCIInputImageKey)
        compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        
        
        removeGreen(image: compositedCIImage!)*/
        
    }
    
    func chromaKeyFilter(fromHue: CGFloat, toHue: CGFloat) -> CIFilter?
    {
        // 1
        let size = 64
        var cubeRGB = [Float]()
        
        // 2
        for z in 0 ..< size {
            let blue = CGFloat(z) / CGFloat(size-1)
            for y in 0 ..< size {
                let green = CGFloat(y) / CGFloat(size-1)
                for x in 0 ..< size {
                    let red = CGFloat(x) / CGFloat(size-1)
                    
                    // 3
                    let hue = getHue(red: red, green: green, blue: blue)
                    let alpha: CGFloat = (hue >= fromHue && hue <= toHue) ? 0: 1
                    
                    // 4
                    cubeRGB.append(Float(red * alpha))
                    cubeRGB.append(Float(green * alpha))
                    cubeRGB.append(Float(blue * alpha))
                    cubeRGB.append(Float(alpha))
                }
            }
        }
        
        let data = Data(buffer: UnsafeBufferPointer(start: &cubeRGB, count: cubeRGB.count))
        
        // 5
        let colorCubeFilter = CIFilter(name: "CIColorCube", parameters: ["inputCubeDimension": size, "inputCubeData": data])
        return colorCubeFilter
    }
    
    func getHue(red: CGFloat, green: CGFloat, blue: CGFloat) -> CGFloat
    {
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        var hue: CGFloat = 0
        color.getHue(&hue, saturation: nil, brightness: nil, alpha: nil)
        return hue
    }
    
    var sourceCIImageWithoutBackgroundTwo: CIImage!
    
    func removeGreen(image: CIImage){
        
        
        let chromaCIFilter = self.chromaKeyFilter(fromHue: 0.3, toHue: 0.4)
        let ourImage = image
        chromaCIFilter?.setValue(ourImage, forKey: kCIInputImageKey)
        sourceCIImageWithoutBackgroundTwo = chromaCIFilter?.outputImage
        
        
        DispatchQueue.main.async {
            
            self.imageView.image = UIImage(ciImage: self.sourceCIImageWithoutBackgroundTwo!)
            
            self.loadingLoaderBackground.alpha = 0
            self.loadingLoader.stopAnimating()
            
            self.loaderBackground.alpha = 0
        }
        
    }
    
    func colorOverlay(image: CIImage){
        
        let backgroundCIImage = CIImage(image: UIImage(named: "goth")!)
        
        
        /*let inputImage = imageView.transform(by: CGAffineTransform(scaleX: backgroundImage.extent.size.width / imageView.image.extent.size.with, y: backgroundImage.extent.size.height / imageView.image.extent.size.height))*/
        
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        var inputImage = sourceCIImageWithoutBackground
        
        
        inputImage = inputImage?.transformed(by: CGAffineTransform(scaleX: backgroundCIImage!.extent.size.width / sourceCIImageWithoutBackground!.extent.size.width, y: backgroundCIImage!.extent.size.height / sourceCIImageWithoutBackground!.extent.size.height))
        
        
        
        let compositor = CIFilter(name:"CISourceOverCompositing")
        compositor?.setValue(inputImage, forKey: kCIInputImageKey)
        compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        
    }
    
    @objc func applyBlackAndWhite(imageName: String){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        let extractedImage = imageView.image!
        //change image to black
        
       
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            let filter: CIFilter = CIFilter(name: "CIColorMonochrome")! //CIPhotoEffectMono -> this makes sure you can still see some features of the image
            filter.setDefaults()
            filter.setValue(CoreImage.CIImage(image: extractedImage)!, forKey: kCIInputImageKey)
            filter.setValue(CIColor.black, forKey: kCIInputColorKey)
            //filter.setValue(UIColor.black.ciColor, forKey: kCIInputColorKey)
            self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: originalImageExtent!)!)
            
            //invert image to white
            
            let invertFilter: CIFilter = CIFilter(name: "CIColorInvert")!
            invertFilter.setDefaults()
            invertFilter.setValue(CoreImage.CIImage(image: self.imageView.image!)!, forKey: kCIInputImageKey)
            self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(invertFilter.outputImage!, from: originalImageExtent!)!)
            
            //removes black and keeps white
            
            let alphaFilter: CIFilter = CIFilter(name: "CIMaskToAlpha")!
            alphaFilter.setDefaults()
            alphaFilter.setValue(CoreImage.CIImage(image: self.imageView.image!)!, forKey: kCIInputImageKey)
            self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(alphaFilter.outputImage!, from: originalImageExtent!)!)
            
            //change alpha to color/image:  and  changes white to alpha 0
            
            //--->
            
            let backgroundCIImage = CIImage(image: UIImage(named: "whiteBackground")!) //change this to original image (which has background removed too) to mask out the hair
            
            let sourceCIImageWithoutBackground = CIImage(image: self.imageView.image!)
            
            let compositor = CIFilter(name:"CISourceOutCompositing")
            compositor?.setValue(backgroundCIImage, forKey: kCIInputImageKey)
            compositor?.setValue(sourceCIImageWithoutBackground, forKey: kCIInputBackgroundImageKey)
            let compositedCIImage = compositor?.outputImage
            
            self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(compositedCIImage!, from: originalImageExtent!)!)
            
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                //self!.invertAndMerge(image: mask)
                //self!.view.isUserInteractionEnabled = true
                self?.mergeBackgroundImageF(imageName: imageName)
                
                
            }
        }
        
        
    }
    
    @objc func applyBlackAndWhiteFromImage(exposureImage: UIImage){
           
           loadingLoaderBackground.alpha = 1
           loadingLoader.startAnimating()
           loaderBackground.alpha = 0.7
           view.isUserInteractionEnabled = false
           
           let originalImageExtent = CIImage(image: imageView.image!)?.extent
           let extractedImage = imageView.image!
           //change image to black
           
          
           
           DispatchQueue.global(qos: .default).async {
               
               // Do heavy work here
               
               let filter: CIFilter = CIFilter(name: "CIColorMonochrome")! //CIPhotoEffectMono -> this makes sure you can still see some features of the image
               filter.setDefaults()
               filter.setValue(CoreImage.CIImage(image: extractedImage)!, forKey: kCIInputImageKey)
               filter.setValue(CIColor.black, forKey: kCIInputColorKey)
               //filter.setValue(UIColor.black.ciColor, forKey: kCIInputColorKey)
               self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: originalImageExtent!)!)
               
               //invert image to white
               
               let invertFilter: CIFilter = CIFilter(name: "CIColorInvert")!
               invertFilter.setDefaults()
               invertFilter.setValue(CoreImage.CIImage(image: self.imageView.image!)!, forKey: kCIInputImageKey)
               self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(invertFilter.outputImage!, from: originalImageExtent!)!)
               
               //removes black and keeps white
               
               let alphaFilter: CIFilter = CIFilter(name: "CIMaskToAlpha")!
               alphaFilter.setDefaults()
               alphaFilter.setValue(CoreImage.CIImage(image: self.imageView.image!)!, forKey: kCIInputImageKey)
               self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(alphaFilter.outputImage!, from: originalImageExtent!)!)
               
               //change alpha to color/image:  and  changes white to alpha 0
               
               //--->
               
               let backgroundCIImage = CIImage(image: UIImage(named: "whiteBackground")!) //change this to original image (which has background removed too) to mask out the hair
               
               let sourceCIImageWithoutBackground = CIImage(image: self.imageView.image!)
               
               let compositor = CIFilter(name:"CISourceOutCompositing")
               compositor?.setValue(backgroundCIImage, forKey: kCIInputImageKey)
               compositor?.setValue(sourceCIImageWithoutBackground, forKey: kCIInputBackgroundImageKey)
               let compositedCIImage = compositor?.outputImage
               
               self.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(compositedCIImage!, from: originalImageExtent!)!)
               
               
               DispatchQueue.main.async { [weak self] in
                   // UI updates must be on main thread
                   
                   
                   //self!.invertAndMerge(image: mask)
                   //self!.view.isUserInteractionEnabled = true
                   self?.mergeBackgroundImageFFromImage(exposureImage: exposureImage)
                   
                   
               }
           }
           
           
       }
    
    @objc func mergeBackgroundImageF(imageName: String){
        //merge images
        
        var backgroundCIImage = CIImage(image: UIImage(named: imageName)!)
        
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        
        DispatchQueue.global(qos: .default).async {
            
            
            
            let inputImage = sourceCIImageWithoutBackground
            
            
            backgroundCIImage = backgroundCIImage?.transformed(by: CGAffineTransform(scaleX: sourceCIImageWithoutBackground!.extent.size.width / backgroundCIImage!.extent.size.width, y: sourceCIImageWithoutBackground!.extent.size.height / backgroundCIImage!.extent.size.height))
            
            
            
            let compositor = CIFilter(name:"CISourceOverCompositing")
            compositor?.setValue(inputImage, forKey: kCIInputImageKey)
            compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
            let compositedCIImage = compositor?.outputImage
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                
                self!.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(compositedCIImage!, from: originalImageExtent!)!) // UIImage(ciImage: compositedCIImage!)
                //self!.invertAndMerge(image: mask)
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0
                self!.saveCurrentImageStateF()
                self!.view.isUserInteractionEnabled = true
                
            }
        }
        
    }
    
    @objc func mergeBackgroundImageFFromImage(exposureImage: UIImage){
        //merge images
        
        var backgroundCIImage = CIImage(image: exposureImage)
        
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            
            
            let inputImage = sourceCIImageWithoutBackground
            
            
            backgroundCIImage = backgroundCIImage?.transformed(by: CGAffineTransform(scaleX: sourceCIImageWithoutBackground!.extent.size.width / backgroundCIImage!.extent.size.width, y: sourceCIImageWithoutBackground!.extent.size.height / backgroundCIImage!.extent.size.height))
            
            
            
            let compositor = CIFilter(name:"CISourceOverCompositing")
            compositor?.setValue(inputImage, forKey: kCIInputImageKey)
            compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
            let compositedCIImage = compositor?.outputImage
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                
                self!.imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(compositedCIImage!, from: originalImageExtent!)!) // UIImage(ciImage: compositedCIImage!)
                //self!.invertAndMerge(image: mask)
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0
                self!.saveCurrentImageStateF()
                self!.view.isUserInteractionEnabled = true
                
            }
        }
        
    }
    
    @objc func openExpusure(){
    
        present(ExposureImagesViewController(), animated: true, completion: nil)
        
    }
    
    func removeSlider(){
        
        topConstraint.isActive = false
        topConstraint.constant = 105
        topConstraint.isActive = true
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    @objc func callTransform(){
        
        let vC = TransformViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callBeautify(){
        
        let vC = BeautifyViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    @objc func callMask(){
        
        let vC = MaskViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callFilter(){
        
        let vC = FilterViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    
    
    @objc func cancelSliderF(){
        
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        removeSlider()
        eraseImage = false
        
    }
    
    @objc func saveSliderF(){
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        removeSlider()
        eraseImage = false
        
    }
    
    var currentPoint: CGPoint!
    var lastPoint: CGPoint!
    
    var eraseImage = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if eraseImage == true || onMask == true {
            
            for touch in touches {
                
                lastPoint = touch.location(in: imageView)
                
            }
            
        }
        
    }
    
    func blurImage(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        
        let ciImg = CIImage(image: imageView.image!)
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            
            
            
            let blur = CIFilter(name: "CIGaussianBlur")
            blur?.setValue(ciImg, forKey: kCIInputImageKey)
            blur?.setValue(10.0, forKey: kCIInputRadiusKey)
            let outputImg = blur?.outputImage
            
            
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
                
                self!.maskImageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(outputImg!, from: originalImageExtent!)!)//UIImage(ciImage: outputImg!)
                self!.drawOnImage()
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0
                self!.view.isUserInteractionEnabled = true
                
                
            }
        }
        
    }
    
    func drawOnImage(){
        
        let hasAlpha = true
        let scale: CGFloat = UIScreen.main.scale // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(maskImageView.frame.size, !hasAlpha, scale)
        
        maskImageView.image!.draw(in: CGRect(x: 0, y: 0, width: maskImageView.frame.size.width, height: maskImageView.frame.size.height))
        
        UIGraphicsGetCurrentContext()?.saveGState()
        UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
        
        maskImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsGetCurrentContext()?.restoreGState()
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if eraseImage == true {
            
            for touch in touches {
                
                currentPoint = touch.location(in: imageView)
                
            }
            
            //UIGraphicsBeginImageContext(imageView.frame.size)
            
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(imageView.frame.size, !hasAlpha, scale)
            
            imageView.image!.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
            
            print(imageView.frame.size)
            
            
            UIGraphicsGetCurrentContext()?.saveGState()
            UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
            UIGraphicsGetCurrentContext()!.setLineCap(CGLineCap.round)
            UIGraphicsGetCurrentContext()?.setLineWidth(CGFloat(lineWidth))
            
            let path = CGMutablePath()
            path.move(to: CGPoint(x: lastPoint.x, y: lastPoint.y), transform: .identity)
            path.addLine(to: CGPoint(x: currentPoint.x, y: currentPoint.y), transform: .identity)
            UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.clear)
            
            UIGraphicsGetCurrentContext()?.addPath(path)
            UIGraphicsGetCurrentContext()?.strokePath()
            
            imageView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsGetCurrentContext()?.restoreGState()
            UIGraphicsEndImageContext()
            
            lastPoint = currentPoint
            
        }else if onMask == true{
            
            for touch in touches {
                
                currentPoint = touch.location(in: imageView)
                
            }
            
            //UIGraphicsBeginImageContext(imageView.frame.size)
            
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(imageView.frame.size, !hasAlpha, scale)
            
            imageView.image!.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
            
            print(imageView.frame.size)
            
            
            UIGraphicsGetCurrentContext()?.saveGState()
            UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
            UIGraphicsGetCurrentContext()!.setLineCap(CGLineCap.round)
            UIGraphicsGetCurrentContext()?.setLineWidth(CGFloat(lineWidth))
            
            let path = CGMutablePath()
            path.move(to: CGPoint(x: lastPoint.x, y: lastPoint.y), transform: .identity)
            path.addLine(to: CGPoint(x: currentPoint.x, y: currentPoint.y), transform: .identity)
            UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.clear)
            
            UIGraphicsGetCurrentContext()?.addPath(path)
            UIGraphicsGetCurrentContext()?.strokePath()
            
            imageView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsGetCurrentContext()?.restoreGState()
            UIGraphicsEndImageContext()
            
            lastPoint = currentPoint
            
        }
        
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        if eraseImage == true || onMask == true {
            
            self.saveCurrentImageStateF()
            
        }else if onMask == true {
            
            let bottomImage = maskImageView.image
            let topImage = imageView.image
            
            let size = imageView.frame.size
            UIGraphicsBeginImageContext(size)
            
            let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            bottomImage!.draw(in: areaSize)
            
            topImage!.draw(in: areaSize, blendMode: .normal, alpha: 0.95)
            
            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imageView.image = newImage
            
            self.saveCurrentImageStateF()
            
        }
    }
    
}
