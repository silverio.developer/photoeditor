//
//  BasicCodeHolderViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/08/31.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit
//import CropViewController

class BasicCodeHolderViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let modeImages = ["crop3", "orientation1", "perspective", "transform", "original"]
    let modeTitles = ["Crop", "Rotate", "Perspective", "Transition", "Original"]
    
    let amountSlider: UISlider = {
        
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 1
        slider.value = 0.7
        
        return slider
        
    }()
    
    var topView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var topShadowLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .lightGray
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var middleView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        
        
        return vw
        
    }()
    
    var bottomView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        
        return vw
        
    }()
    
    var imageBackground: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        //img.backgroundColor = .yellow
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleToFill
        //img.image = UIImage(named: "reach")
        
        
        return img
        
    }()
    
    var imageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var previousImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "backward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    lazy var previousBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(previousF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var forwardBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    var forwardImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "foward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    var nycImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "transform-1")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var menuCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCell.self, forCellWithReuseIdentifier: "MenuId")
        cv.backgroundColor = .clear
        
        return cv
        
    }()
    
    var compareView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.cornerRadius = 8
        vw.layer.borderColor = UIColor.black.cgColor
        vw.layer.borderWidth = 2
        
        
        return vw
        
    }()
    
    var compareImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "compare")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    let compareLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .white
        lbl.text = "Long Press To Compare"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 15)
        lbl.layer.masksToBounds = true
        lbl.layer.cornerRadius = 8
        lbl.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        return lbl
        
    }()
    
    lazy var saveCurrentImage: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveCurrentImageF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var cancelTranform: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelTransoformF), for: .touchUpInside)
        
        return btn
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func addGestureRecognizer(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        
        longPress.delegate = self
        
        imageView.addGestureRecognizer(longPress)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var heightConstraint: NSLayoutConstraint!
    
//    var cropViewController: CropViewController!
    
    func setup(){
        
        var widthSize = view.frame.width - 16
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                widthSize = view.frame.width / 1.30
                
            case 1334:
                print("iPhone 6/6S/7/8")
                widthSize = view.frame.width / 1.25
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                widthSize = view.frame.width / 1.25
                
            case 2436:
                print("iPhone X, XS")
                
            case 2688:
                print("iPhone XS Max")
                
            case 1792:
                print("iPhone XR")
                
            default:
                print("Unknown")
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            
            widthSize = view.frame.width / 1.9
            
        }
        
        addGestureRecognizer()
        
        
        view.backgroundColor = UIColor.init(red: 212/255, green: 213/255, blue: 214/255, alpha: 1)
        
        view.addSubview(topView)
        view.addSubview(bottomView)
        view.addSubview(imageBackground)
        view.addSubview(imageView)
        view.addSubview(previousBtn)
        view.addSubview(forwardBtn)
        view.addSubview(previousImage)
        view.addSubview(forwardImage)
        
        bottomView.addSubview(menuCV)
        
        menuCV.delegate = self
        menuCV.dataSource = self
        
        //view.addSubview(compareView)
        //compareView.addSubview(compareImageView)
        view.addSubview(compareLbl)
        
        topView.addSubview(nycImageView)
        
        topView.addSubview(saveCurrentImage)
        topView.addSubview(cancelTranform)
        
        
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: view.frame.width / 5 + 25).isActive = true
        
        imageBackground.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        imageBackground.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -2).isActive = true
        imageBackground.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        imageBackground.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -2).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageBackground.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBackground.addSubview(blurEffectView)
        
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        //imageView.frame.size.width = view.frame.size.width - 16
        //imageView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -8).isActive = true
        
        
        
        forwardBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 35).isActive = true
        forwardBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        previousBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -35).isActive = true
        previousBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        forwardImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.centerXAnchor.constraint(equalTo: forwardBtn.centerXAnchor).isActive = true
        forwardImage.centerYAnchor.constraint(equalTo: forwardBtn.centerYAnchor).isActive = true
        
        previousImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.centerXAnchor.constraint(equalTo: previousBtn.centerXAnchor).isActive = true
        previousImage.centerYAnchor.constraint(equalTo: previousBtn.centerYAnchor).isActive = true
        
        menuCV.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 15).isActive = true
        menuCV.heightAnchor.constraint(equalToConstant: view.frame.width / 5).isActive = true
        menuCV.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        menuCV.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
        
        /*compareView.heightAnchor.constraint(equalToConstant: 60).isActive = true
         compareView.widthAnchor.constraint(equalToConstant:60).isActive = true
         compareView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 5).isActive = true
         compareView.topAnchor.constraint(equalTo: menuCV.bottomAnchor, constant: 5).isActive = true*/
        
        compareLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        compareLbl.widthAnchor.constraint(equalToConstant: 195).isActive = true
        compareLbl.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -25).isActive = true
        compareLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
        nycImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nycImageView.widthAnchor.constraint(equalToConstant: view.frame.width - 55).isActive = true
        nycImageView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 40).isActive = true
        nycImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        saveCurrentImage.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -15).isActive = true
        saveCurrentImage.heightAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.widthAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        cancelTranform.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 15).isActive = true
        cancelTranform.heightAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.widthAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        /*compareImageView.leftAnchor.constraint(equalTo: compareView.leftAnchor).isActive = true
         compareImageView.rightAnchor.constraint(equalTo: compareView.rightAnchor).isActive = true
         compareImageView.topAnchor.constraint(equalTo: compareView.topAnchor).isActive = true
         compareImageView.bottomAnchor.constraint(equalTo: compareLbl.topAnchor).isActive = true*/
        
        //menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        
        heightConstraint.isActive = false
        
        let ratio = imageView.image!.size.width / imageView.image!.size.height
        
        print(imageView.frame.width)
        let newHeight = widthSize / ratio
        heightConstraint.constant = newHeight
        heightConstraint.isActive = true
        
        view.layoutIfNeeded()
        
        //processImage()
        //changeHairColor()
        //peopleApi()
        //mergePictures()
        //applyBlackAndWhite()
        
        //rotate image:
        
        /*UIView.animate(withDuration: 2.0, animations: {
         self.imageView.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
         })*/
        
    }
    
    @objc func previousF(){
        
        print("Previous")
    }
    
    @objc func forwardF(){
        
        print("Forward")
    }
    
    func presentCropper(){
        
        //cropViewController = CropViewController(image: imageView.image!)
        //cropViewController.delegate = self
        
        
        //present(cropViewController, animated: true, completion: nil)
        
    }
    
    /*func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            
            self.imageView.image = image
            
        }
        
    }*/
    
    @objc func cancelTransoformF(){
        
        //cancel transforming image
        
        dismiss(animated: true, completion: nil)
        
        /* let pngImageData = imageView.image!.pngData()
         
         //let imageData = imageView.image?.jpegData(compressionQuality: 0.65)
         let compressedImage = UIImage(data: pngImageData!)
         
         UIImageWriteToSavedPhotosAlbum(compressedImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil) */
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Image saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    let imagePickerController = UIImagePickerController()
    
    @objc func saveCurrentImageF(){
        
        //save current state image
        
        dismiss(animated: true, completion: nil)
        
        /*imagePickerController.delegate = self
         imagePickerController.allowsEditing = false
         
         imagePickerController.sourceType = .photoLibrary
         present(imagePickerController, animated: true, completion: nil)*/
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            
            return self.imagePickerControllerDidCancel(picker)
            
        }
        
        self.imageView.image = image
        //peopleApi()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        picker.delegate = nil
    }
    
    var longPressed = false
    
    @objc func handleLongPress(){
        
        
        
        if longPressed == false {
            
            longPressed = true
            
        }else {
            
            longPressed = false
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = menuCV.dequeueReusableCell(withReuseIdentifier: "MenuId", for: indexPath) as! MenuCell
        
        cell.modeImage.image = UIImage(named: modeImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        //cell.modeImage.tintColor = .black
        //cell.modeTitle.textColor = .black
        
        
        cell.modeTitle.text = modeTitles[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            presentCropper()
            
        }else if indexPath.row == 1 {
            
            perform(#selector(callTransform), with: nil, afterDelay: 0.35)
            
        }else if indexPath.row == 2 {
            
            perform(#selector(callBeautify), with: nil, afterDelay: 0.35)
            
        }else if indexPath.row == 3 {
            
            perform(#selector(callMask), with: nil, afterDelay: 0.35)
            
        }else if indexPath.row == 4 {
            
            perform(#selector(callFilter), with: nil, afterDelay: 0.35)
            
        }
        
    }
    
    @objc func callTransform(){
        
        let vC = TransformViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callBeautify(){
        
        let vC = BeautifyViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    @objc func callMask(){
        
        let vC = MaskViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callFilter(){
        
        let vC = FilterViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: view.frame.width / 5, height: view.frame.width / 5)
        
        return size
        
    }
    
    
}
