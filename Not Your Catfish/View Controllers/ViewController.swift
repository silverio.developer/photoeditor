//
//  ViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/08/28.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit
import Fritz
import YUCIHighPassSkinSmoothing
import CropViewController
import NVActivityIndicatorView
import Lottie
import MessageUI
import StoreKit


class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    let context = CIContext(options: [CIContextOption.workingColorSpace: CGColorSpaceCreateDeviceRGB()])
    let filter = YUCIHighPassSkinSmoothing()

    let modeImages = ["paw", "dolphin", "catUp", "mask", "filter"]
    let modeTitles = ["Home", "Transform", "Beautify", "Mask", "Filter"]
    
    private enum MIMEType: String {
        
        case jpg = "image/jpeg"
        case png = "image/png"
        case doc = "application/msword"
        case ppt = "application/vnd.ms-powerpoint"
        case html = "text/html"
        case pdf = "application/pdf"
        
        init?(type:String) {
            
            switch type.lowercased() {
                
            case "jpg": self = .jpg
            case "png": self = .png
            case "doc": self = .doc
            case "ppt": self = .ppt
            case "html": self = .html
            case "pdf": self = .pdf
            default: return nil
                
            }
        }
        
    }
    
    let amountSlider: UISlider = {
        
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 1
        slider.value = 0.7
        
        return slider
        
    }()
    
    var topView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var topShadowLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .lightGray
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var middleView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        
        
        return vw
        
    }()
    
    var bottomView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var imageBackground: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        //img.backgroundColor = .yellow
        img.image = UIImage(named: "defaultImage")
        img.contentMode = .scaleAspectFit
        //img.image = UIImage(named: "reach")
        
        
        return img
        
    }()
    
    var imageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "defaultImage")
        img.contentMode = .scaleAspectFit
        
        img.layer.zPosition = 3
        
        
        return img
        
    }()
    
    var previousImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "backward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        img.layer.zPosition = 4
        
        
        return img
        
    }()
    
    lazy var previousBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(previousF), for: .touchUpInside)
        
        btn.layer.zPosition = 4
        
        return btn
        
    }()
    
    lazy var forwardBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        btn.layer.zPosition = 4
        
        return btn
        
    }()
    
    var forwardImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "foward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        img.layer.zPosition = 4
        
        
        return img
        
    }()
    
    var nycImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "nyc")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var menuCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCell.self, forCellWithReuseIdentifier: "MenuId")
        cv.backgroundColor = .clear
        
        return cv
        
    }()
    
    var compareView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.cornerRadius = 8
        vw.layer.borderColor = UIColor.black.cgColor
        vw.layer.borderWidth = 2
        
        vw.layer.zPosition = 4
        
        
        return vw
        
    }()
    
    var compareImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "compare")
        img.contentMode = .scaleAspectFit
        
        img.layer.zPosition = 4
        
        return img
        
    }()
    
    let compareLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .white
        lbl.text = "Long Press To Compare"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 15)
        lbl.layer.masksToBounds = true
        lbl.layer.cornerRadius = 8
        lbl.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        lbl.layer.zPosition = 4
        
        return lbl
        
    }()
    
    lazy var newImageBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "photo-camera")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(importImage), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var exportBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "export3")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(callSaveShareAlert), for: .touchUpInside)
        
        return btn
        
    }()
    
    
    var loaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 18
        vw.layer.masksToBounds = true
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoader: NVActivityIndicatorView = {
        
        let loader = NVActivityIndicatorView(frame: .zero)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.type = .ballTrianglePath
        loader.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.color = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.layer.zPosition = 5
        loader.alpha = 0
        
        return loader
        
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
    }
    
    private lazy var visionModel = FritzVisionHairSegmentationModelAccurate()
    
    func addGestureRecognizer(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        
        longPress.delegate = self
        
        imageView.addGestureRecognizer(longPress)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var panGes: UIPanGestureRecognizer!
    
    func addPanGestureRecognizer(){
        
        panGes = UIPanGestureRecognizer(target: self, action: #selector(panGesF))
        
        panGes.delegate = self
        
        imageView.addGestureRecognizer(panGes)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var anView: AnimationView = {
        
        let vw = AnimationView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        
        vw.animation = Animation.filepath("/Users/thapelo/Downloads/Not Your Catfish/Not Your Catfish/2478-check.json")
        vw.animationSpeed = 0.7
        vw.layer.zPosition = 10
        vw.alpha = 0
        
        return vw
        
    }()
    
    var heightConstraint: NSLayoutConstraint!
    
    var cropViewController: CropViewController!
    
    var widthSize: CGFloat!
    
    func setup(){
        
        widthSize = view.frame.width - 16
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                widthSize = view.frame.width / 1.30
                
            case 1334:
                print("iPhone 6/6S/7/8")
                widthSize = view.frame.width / 1.25
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                widthSize = view.frame.width / 1.25
                
            case 2436:
                print("iPhone X, XS")
                
            case 2688:
                print("iPhone XS Max")
                
            case 1792:
                print("iPhone XR")
                
            default:
                print("Unknown")
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            
            widthSize = view.frame.width / 1.9
            
        }
        
        addGestureRecognizer()
        
        
        view.backgroundColor = UIColor.init(red: 212/255, green: 213/255, blue: 214/255, alpha: 1)
        
        view.addSubview(topView)
        view.addSubview(bottomView)
        view.addSubview(imageBackground)
        view.addSubview(imageView)
        view.addSubview(previousBtn)
        view.addSubview(forwardBtn)
        view.addSubview(previousImage)
        view.addSubview(forwardImage)
        
        bottomView.addSubview(menuCV)
        
        
        
        menuCV.delegate = self
        menuCV.dataSource = self
        
        //view.addSubview(compareView)
        //compareView.addSubview(compareImageView)
        view.addSubview(compareLbl)
        
        topView.addSubview(nycImageView)
        
        topView.addSubview(newImageBtn)
        topView.addSubview(exportBtn)
        
        view.addSubview(loaderBackground)
        view.addSubview(loadingLoaderBackground)
        view.addSubview(loadingLoader)
       
        view.addSubview(anView)
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: view.frame.width / 5 + 25).isActive = true
        
        imageBackground.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        imageBackground.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -2).isActive = true
        imageBackground.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        imageBackground.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -2).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageBackground.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBackground.addSubview(blurEffectView)
        
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        //imageView.frame.size.width = view.frame.size.width - 16
        //imageView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -8).isActive = true
        
        
        
        forwardBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 35).isActive = true
        forwardBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        previousBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -35).isActive = true
        previousBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        forwardImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.centerXAnchor.constraint(equalTo: forwardBtn.centerXAnchor).isActive = true
        forwardImage.centerYAnchor.constraint(equalTo: forwardBtn.centerYAnchor).isActive = true
        
        previousImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.centerXAnchor.constraint(equalTo: previousBtn.centerXAnchor).isActive = true
        previousImage.centerYAnchor.constraint(equalTo: previousBtn.centerYAnchor).isActive = true
        
        menuCV.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 15).isActive = true
        menuCV.heightAnchor.constraint(equalToConstant: view.frame.width / 5).isActive = true
        menuCV.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        menuCV.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
        
        /*compareView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        compareView.widthAnchor.constraint(equalToConstant:60).isActive = true
        compareView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 5).isActive = true
        compareView.topAnchor.constraint(equalTo: menuCV.bottomAnchor, constant: 5).isActive = true*/
        
        compareLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        compareLbl.widthAnchor.constraint(equalToConstant: 195).isActive = true
        compareLbl.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -25).isActive = true
        compareLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
    
        nycImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -20).isActive = true
        nycImageView.widthAnchor.constraint(equalToConstant: view.frame.width - 55).isActive = true
        nycImageView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 45).isActive = true
        nycImageView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        newImageBtn.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -15).isActive = true
        newImageBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        newImageBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        newImageBtn.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        exportBtn.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 15).isActive = true
        exportBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        exportBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        exportBtn.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        
        
        /*compareImageView.leftAnchor.constraint(equalTo: compareView.leftAnchor).isActive = true
        compareImageView.rightAnchor.constraint(equalTo: compareView.rightAnchor).isActive = true
        compareImageView.topAnchor.constraint(equalTo: compareView.topAnchor).isActive = true
        compareImageView.bottomAnchor.constraint(equalTo: compareLbl.topAnchor).isActive = true*/
        
        //menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        
        updateImageConstraints()
        
        loaderBackground.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        loaderBackground.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        loaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoaderBackground.widthAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.heightAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoader.widthAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.heightAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        anView.widthAnchor.constraint(equalToConstant: 155).isActive = true
        anView.heightAnchor.constraint(equalToConstant: 155).isActive = true
        anView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        anView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        
        UserDefaults.standard.set(nil, forKey: "currentImageNumber")
        UserDefaults.standard.set(nil, forKey: "maxImageNumber")
        
       
        
    }
    
    func blurImage(){
        
        let ciImg = CIImage(image: UIImage(named: "afro")!)
        
        let blur = CIFilter(name: "CIGaussianBlur")
        blur?.setValue(ciImg, forKey: kCIInputImageKey)
        blur?.setValue(10.0, forKey: kCIInputRadiusKey)
        if let outputImg = blur?.outputImage {
            
            
            self.imageView.image = UIImage(ciImage: outputImg)
            
        }
        
    }
    
    @objc func panGesF(){
        
        let scaleFactor = UIScreen.main.scale
        
        let location = panGes.location(in: imageView)
        let x = location.x * scaleFactor
        let y = imageView.bounds.height * scaleFactor - location.y * scaleFactor
        
        
        let perspectiveTransform = CIFilter(name: "CIPerspectiveTransform")!
        
        let ciImage = CIImage(image: UIImage(named: "afro")!)
        
        perspectiveTransform.setValue(CIVector(x: x, y: y),forKey: "inputTopLeft")
        perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 510)),forKey: "inputTopRight")
        perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 140)), forKey: "inputBottomRight")
        perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 140)), forKey: "inputBottomLeft")
        
        
        
        perspectiveTransform.setValue(ciImage, forKey: kCIInputImageKey)
        
        imageView.image = UIImage(ciImage: perspectiveTransform.outputImage!)
        
       
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if loadingLoader.isAnimating == true {
            
            loaderBackground.alpha = 0
            loadingLoaderBackground.alpha = 0
            loadingLoader.stopAnimating()
            
        }
        
        menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        
        if let currentImageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            if currentImageNumber >= 1 {
                
                if let imageData = UserDefaults.standard.object(forKey: "image\(currentImageNumber)") as? NSData {
                    
                    print("view will appear")
                    self.imageView.image = UIImage(data: imageData as Data)
                    
                }
                
            }
            
        }
        
        updateImageConstraints()
        
        view.layoutIfNeeded()
    }
    
    func presentCropper(){
        
        cropViewController = CropViewController(image: imageView.image!)
        cropViewController.delegate = self
        
        
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            
            self.imageView.image = image
            
            self.updateImageConstraints()
            
        }
        
    }
    
    @objc func updateImageConstraints(){
        
        self.heightConstraint.isActive = false
        
        let ratio = self.imageView.image!.size.width / self.imageView.image!.size.height
        
        let newHeight = self.widthSize / ratio
        self.heightConstraint.constant = newHeight
        self.heightConstraint.isActive = true
        
        self.view.layoutIfNeeded()
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropImageToRect rect: CGRect, angle: Int) {
        
        
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        print("Circular Imagw")
    }
    
    //Resizing an image:
    
    func resizedImage(for size: CGSize) -> UIImage? {
        guard let image = imageView.image else {
            return nil
        }
        
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    func imageWith(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            imageView.draw(CGRect.init(origin: CGPoint.zero, size: newSize))
        }
        
        return image
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    @objc func previousF(){
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            if imageNumber != 1 {
                
                let newImageNumber = imageNumber - 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "image\(newImageNumber)") as? NSData {
                    
                    UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                    
                    imageView.image = UIImage(data: imageData as Data)
                    
                }
                
            }
            
        }
        
        updateImageConstraints()
        
    }
    
    @objc func forwardF(){
        
        print("ayt")
        
        if let max = UserDefaults.standard.object(forKey: "maxImageNumber") as? Int {
            
            print("maxxed:")
            print(max)
            
            if let current = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                print("currented:")
                print(current)
                
                if max >= current + 1 {
                    
                    print("through")
                    
                    let newCurrent = current + 1
                    
                    UserDefaults.standard.set(newCurrent, forKey: "currentImageNumber")
                    
                    if let imageData = UserDefaults.standard.object(forKey: "image\(newCurrent)") as? NSData {
                        
                        UserDefaults.standard.set(imageData, forKey: "image\(newCurrent)")
                        
                        imageView.image = UIImage(data: imageData as Data)
                        
                        self.updateImageConstraints()
                        
                    }
                    
                    
                }
                
            }
            
        }
        
        
    }
    
    func applyBlackAndWhite(){
        
        let originalImageExtent = CIImage(image: imageView.image!)?.extent
        
        //change image to black
        
        let filter: CIFilter = CIFilter(name: "CIColorMonochrome")! //CIPhotoEffectMono -> this makes sure you can still see some features of the image
        filter.setDefaults()
        filter.setValue(CoreImage.CIImage(image: imageView.image!)!, forKey: kCIInputImageKey)
        filter.setValue(CIColor.black, forKey: kCIInputColorKey)
        //filter.setValue(UIColor.black.ciColor, forKey: kCIInputColorKey)
        imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: originalImageExtent!)!)
        
      
        
        //invert image to white
        
        let invertFilter: CIFilter = CIFilter(name: "CIColorInvert")!
        invertFilter.setDefaults()
        invertFilter.setValue(CoreImage.CIImage(image: imageView.image!)!, forKey: kCIInputImageKey)
        imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(invertFilter.outputImage!, from: originalImageExtent!)!)
        
        //removes black and keeps white
        
        let alphaFilter: CIFilter = CIFilter(name: "CIMaskToAlpha")!
        alphaFilter.setDefaults()
        alphaFilter.setValue(CoreImage.CIImage(image: imageView.image!)!, forKey: kCIInputImageKey)
        imageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(alphaFilter.outputImage!, from: originalImageExtent!)!)
        
        //change alpha to color/image:  and  changes white to alpha 0
        
        //--->
        
        let backgroundCIImage = CIImage(image: UIImage(named: "whiteBackground")!) //change this to original image (which has background removed too) to mask out the hair
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        let compositor = CIFilter(name:"CISourceOutCompositing")
        compositor?.setValue(backgroundCIImage, forKey: kCIInputImageKey)
        compositor?.setValue(sourceCIImageWithoutBackground, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        imageView.image = UIImage(ciImage: compositedCIImage!)
        
        //--->
        
       // mergeBackgroundImageF()
        
        //change background
        
        /*let backgroundCIImage = CIImage(image: UIImage(named: "afro")!)
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        let compositor = CIFilter(name:"CISourceOverCompositing")
        compositor?.setValue(sourceCIImageWithoutBackground, forKey: kCIInputImageKey)
        compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        imageView.image = UIImage(ciImage: compositedCIImage!)*/
        
    }
    
    func mergeBackgroundImageF(){
        //merge images
        
        let backgroundCIImage = CIImage(image: UIImage(named: "autumn")!)
        
        
        /*let inputImage = imageView.transform(by: CGAffineTransform(scaleX: backgroundImage.extent.size.width / imageView.image.extent.size.with, y: backgroundImage.extent.size.height / imageView.image.extent.size.height))*/
        
        
        let sourceCIImageWithoutBackground = CIImage(image: imageView.image!)
        
        var inputImage = sourceCIImageWithoutBackground
        
        
        inputImage = inputImage?.transformed(by: CGAffineTransform(scaleX: backgroundCIImage!.extent.size.width / sourceCIImageWithoutBackground!.extent.size.width, y: backgroundCIImage!.extent.size.height / sourceCIImageWithoutBackground!.extent.size.height))
        

        
        let compositor = CIFilter(name:"CISourceOverCompositing")
        compositor?.setValue(inputImage, forKey: kCIInputImageKey)
        compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        imageView.image = UIImage(ciImage: compositedCIImage!)
        
    }
    
    @objc func callSaveShareAlert(){
        
        
        let alert = UIAlertController(title: "Save or Share", message: nil, preferredStyle: .actionSheet)
        
        let save = UIAlertAction(title: "Save to Gallery", style: .default) { (action) in
            
            self.saveImageToPhotoLibrary()
            
        }
        
        let share = UIAlertAction(title: "Save and Share", style: .default) { (action) in
            
            self.saveImageToPhotoLibrary()
            self.shareF()
            
        }
        
        let competition = UIAlertAction(title: "iPhone 11 Competition", style: .default) { (action) in
            
            self.saveImageToPhotoLibrary()
            self.showMailComposer()
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(save)
        alert.addAction(share)
        //alert.addAction(competition)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showMailComposer(){
        
        if MFMailComposeViewController.canSendMail() {
            
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setCcRecipients(["garlictechnologies@gmail.com"])
            mail.setSubject("iPhone 11 Competition")
            mail.setMessageBody("Below is attached my Not Your Catfish edited photo, that I want to use to enter for the iPhone 11 Competition", isHTML: false)
            
            let imageData: NSData = imageView.image!.pngData()! as NSData
            
            mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
            
            present(mail, animated: true, completion: nil)
            
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func shareF(){
    
        let activityVC = UIActivityViewController(activityItems: [self.imageView.image as Any], applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        
        
        present(activityVC, animated: true, completion: nil)
        
    }
    
    
    @objc func saveImageToPhotoLibrary(){
        
        let pngImageData = imageView.image!.pngData()
        
        //let imageData = imageView.image?.jpegData(compressionQuality: 0.65)
        let compressedImage = UIImage(data: pngImageData!)
        
        UIImageWriteToSavedPhotosAlbum(compressedImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    
        loaderBackground.alpha = 0.7
        loadingLoaderBackground.alpha = 1
        loadingLoader.alpha = 1
        loadingLoader.startAnimating()
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if error == nil {
            
            anView.alpha = 1
            anView.play { (true) in
                
                self.loadingLoaderBackground.alpha = 0
                self.anView.alpha = 0
                
            }
            loaderBackground.alpha = 0
            loadingLoaderBackground.alpha = 0
            loadingLoader.stopAnimating()
            
            if (UserDefaults.standard.object(forKey: "firstImageSavedRated") as? Bool) != nil {
                
                let ac = UIAlertController(title: "Saved!", message: "Image saved to your photos.", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                
            }else {
                
                perform(#selector(rateF), with: nil, afterDelay: 0.7)
                
                UserDefaults.standard.set(true, forKey: "firstImageSavedRated")
                
            }
            
            
            //present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    @objc func rateF(){
        
        if #available(iOS 10.3, *) {
            
            SKStoreReviewController.requestReview()
            
        }else {
            
            if let url = URL(string: "https://apps.apple.com/id1477095259?action=write-review/") {
                
                UIApplication.shared.open(url, options: [:]) { (result) in
                    
                    if result {
                        
                        
                    }else {
                        
                        
                        
                    }
                    
                }
                
                
            }
            
        }
        
    }
    
    @objc func didFinishSaving(){
        
        print("image saved")
        
    }
    
    let imagePickerController = UIImagePickerController()
    
    @objc func callAlert(){
        
        let alert = UIAlertController(title: "Select Source Type", message: nil, preferredStyle: .actionSheet)
        
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            
            self.imagePickerController.delegate = self
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
            
            self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            
            self.imagePickerController.delegate = self
            self.imagePickerController.allowsEditing = false
            self.imagePickerController.sourceType = .camera
            self.present(self.imagePickerController, animated: true, completion: nil)
            
            self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
            self.menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
            
        }
        
        alert.addAction(photoLibrary)
        alert.addAction(camera)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func importImage(){
        
        callAlert()
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            
            return self.imagePickerControllerDidCancel(picker)
            
        }
        
        self.imageView.image = image
        self.imageBackground.image = image
        saveInitialImage(image: image)
        
        //mergeBackgroundImageF()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func saveInitialImage(image: UIImage) {
        
        let image = image
        let imageData: NSData = image.pngData()! as NSData
        
        
        UserDefaults.standard.set(1, forKey: "currentImageNumber")
        UserDefaults.standard.set(imageData, forKey: "image1")
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        picker.delegate = nil
    }
    
    var longPressed = false
    
    var currentImageDisplayed: UIImage!
    
    var longPressedStock = false
    
    @objc func handleLongPress(){
        
        
        
        if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) == nil {
            
            
            //show original image
            
            if longPressedStock == false {
                
                
                
                
                longPressedStock = true
                
                
            }else {
                
                //show edited image
                
                
                
                longPressedStock = false
                
            }
            
        }else {
            
            if longPressed == false {
                
                currentImageDisplayed = imageView.image
                
                if let originalImage = UserDefaults.standard.object(forKey: "image1") as? NSData {
                    
                    self.imageView.image = UIImage(data: originalImage as Data)
                    
                    self.updateImageConstraints()
                    
                }
                
                longPressed = true
                
                
            }else {
                
                imageView.image = currentImageDisplayed
                
                self.updateImageConstraints()
                
                longPressed = false
                
            }
            
            
            
        }
        
    }
    
    var currentPoint: CGPoint!
    var lastPoint: CGPoint!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
        
        
    }
    
    func createVector(_ point:CGPoint, image:CIImage) -> CIVector {
        return CIVector(x: point.x, y: image.extent.height - point.y)
    }
   
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        
    }
    
    func mergePictures(){
        
        
        let inputCIImage = CIImage(image: imageView.image!)
        let backgroundCIImage = CIImage(image: UIImage(named: "whiteBackground")!)
            
        let compositor = CIFilter(name:"CISourceInCompositing")
        compositor?.setValue(inputCIImage, forKey: kCIInputImageKey)
        compositor?.setValue(backgroundCIImage, forKey: kCIInputBackgroundImageKey)
        let compositedCIImage = compositor?.outputImage
        imageView.image = UIImage(ciImage: compositedCIImage!)
        
        
        
    }
    
    var processedImage: UIImage?
    
    func processImage() {
        
        let inputCIImage = CIImage(image: imageView.image!)
        
        
        print(self.filter.outputKeys)
        self.filter.inputImage = inputCIImage
        self.filter.inputAmount = 0.7 as NSNumber
        self.filter.inputRadius = 7.0 * inputCIImage!.extent.width/750.0 as NSNumber
        let outputCIImage = filter.outputImage!
        
        let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: imageView.image!.scale, orientation: self.imageView.image!.imageOrientation)
        
        self.processedImage = outputUIImage
        self.imageView.image = self.processedImage
        
        
    }
    
    var clippingScoresAbove: Double { return 0.7 }
    
    
    var zeroingScoresBelow: Double { return 0.3 }
    
    
    var opacity: CGFloat { return 0.7 }
    
    
    var blendMode: CIBlendKernel { return .softLight }
    
    
    var color: UIColor { return .green }
    
    func changeHairColor(){
        
        let image = FritzVisionImage(image: imageView.image!)
        
        image.metadata = FritzVisionImageMetadata()
        image.metadata?.orientation = .up
        
        guard let result = try? visionModel.predict(image),
            let mask = result.buildSingleClassMask(
                forClass: FritzVisionHairClass.hair,
                clippingScoresAbove: clippingScoresAbove,
                zeroingScoresBelow: zeroingScoresBelow,
                resize: false,
                color: color)
            else { return }
        
        let blended = image.blend(
            withMask: mask,
            blendKernel: blendMode,
            opacity: opacity
        )
        
        DispatchQueue.main.async {
            self.imageView.image = blended
        }
        
    }
    
    let peopleModel = FritzVisionPeopleSegmentationModelAccurate()
    
    func peopleApi(){
        
        let image = FritzVisionImage(image: imageView.image!)
        
        guard let result = try? peopleModel.predict(image),
            let rotatedImage = image.rotate()
            else { return }
        
        let background = UIImage(pixelBuffer: rotatedImage)
        
        let mask = result.buildSingleClassMask(
            forClass: FritzVisionPeopleClass.person,
            clippingScoresAbove: 0.7,
            zeroingScoresBelow: 0.25
        )
        
        //use mask in cimasktoalpha --> it changes black content into transparent content ??
        
        let clippedMaskImage = image.masked(with: mask!)
        
        DispatchQueue.main.async {
            //self.cameraView.image = background
            self.imageView.image = clippedMaskImage
            self.applyBlackAndWhite()
            //self.mergePictures()
            //self.backgroundView.image = background
        }
        
        
    }
    
    func doubleMask(){
        
        let image = FritzVisionImage(image: imageView.image!)
        
        guard let result = try? peopleModel.predict(image),
            let rotatedImage = image.rotate()
            else { return }
        
        let background = UIImage(pixelBuffer: rotatedImage)
        
        let mask = result.buildSingleClassMask(
            forClass: FritzVisionPeopleClass.person,
            clippingScoresAbove: 0.7,
            zeroingScoresBelow: 0.25
        )
        
        //use mask in cimasktoalpha --> it changes black content into transparent content ??
        
        let clippedMaskImage = image.masked(with: mask!)
        
        DispatchQueue.main.async {
            //self.cameraView.image = background
            self.imageView.image = clippedMaskImage
            self.mergePictures()
            //self.backgroundView.image = background
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = menuCV.dequeueReusableCell(withReuseIdentifier: "MenuId", for: indexPath) as! MenuCell
        
        cell.modeImage.image = UIImage(named: modeImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        
        
        
        cell.modeTitle.text = modeTitles[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) != nil {
                
                
                
            }else {
                
                callAlert()
                
            }
            
        }else if indexPath.row == 1 {
            
            if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) != nil {
                
                perform(#selector(callTransform), with: nil, afterDelay: 0.35)
                
            }else {
                
                callAlert()
                
            }
            
            
            
        }else if indexPath.row == 2 {
            
            if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) != nil {
                
                perform(#selector(callBeautify), with: nil, afterDelay: 0.35)
                
            }else {
                
                callAlert()
                
            }
            
            
            
        }else if indexPath.row == 3 {
            
            if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) != nil {
                
                perform(#selector(callMask), with: nil, afterDelay: 0.35)
                
            }else {
                
                callAlert()
                
            }
            
            
            
        }else if indexPath.row == 4 {
            
            if (UserDefaults.standard.object(forKey: "currentImageNumber") as? Int) != nil {
                
                perform(#selector(callFilter), with: nil, afterDelay: 0.35)
                
            }else {
                
                callAlert()
                
            }
            
        }
        
    }
    
    @objc func callTransform(){
        
        let vC = TransformViewController()
        vC.modalPresentationStyle = .fullScreen
        vC.imageBackground.image = imageBackground.image
        vC.imageView.image = imageView.image
        
        let image = imageView.image
        
        
        let imageData: NSData = image!.pngData()! as NSData
        
        
        if let number = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "firstOpenNumber")
            
        }else{
            
            UserDefaults.standard.set(1, forKey: "firstOpenNumber")
            
        }
        
        UserDefaults.standard.set(imageData, forKey: "firstOpen")
        
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callBeautify(){
        
        let vC = BeautifyViewController()
        
        vC.modalPresentationStyle = .fullScreen
        vC.imageBackground.image = imageBackground.image
        vC.imageView.image = imageView.image
        
        
        let image = imageView.image
        let imageData: NSData = image!.pngData()! as NSData
        
        if let number = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "firstOpenNumber")
            
        }else{
            
            UserDefaults.standard.set(1, forKey: "firstOpenNumber")
            
        }
        
        UserDefaults.standard.set(imageData, forKey: "firstOpen")
        
        present(vC, animated: true, completion: nil)
    }
    
    @objc func callMask(){
        
        let vC = MaskViewController()
        vC.modalPresentationStyle = .fullScreen
        vC.imageBackground.image = imageBackground.image
        vC.imageView.image = imageView.image
        
        let image = imageView.image
        let imageData: NSData = image!.pngData()! as NSData
        
        if let number = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "firstOpenNumber")
            
        }else{
            
            UserDefaults.standard.set(1, forKey: "firstOpenNumber")
            
        }
        
        UserDefaults.standard.set(imageData, forKey: "firstOpen")
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callFilter(){
        
        loaderBackground.alpha = 0.7
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        
        let vC = FilterViewController()
        vC.modalPresentationStyle = .fullScreen
        vC.imageBackground.image = imageBackground.image
        vC.imageView.image = imageView.image
        
        let image = imageView.image
        let imageData: NSData = image!.pngData()! as NSData
        
        if let number = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "firstOpenNumber")
            
        }else{
            
            UserDefaults.standard.set(1, forKey: "firstOpenNumber")
            
        }
        
        UserDefaults.standard.set(imageData, forKey: "firstOpen")
        
        present(vC, animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: view.frame.width / 5, height: view.frame.width / 5)
        
        return size
        
    }

}

class MenuCell: UICollectionViewCell {
    
    let modeImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = .black
        img.contentMode = .scaleAspectFit
        
        return img
        
    }()
    
    let modeTitle: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .black
        lbl.text = "Smooth"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 13)
        
        return lbl
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(modeImage)
        addSubview(modeTitle)
        
        
        modeTitle.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        modeTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        modeTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        modeTitle.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        modeImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        modeImage.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        modeImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        modeImage.bottomAnchor.constraint(equalTo: modeTitle.topAnchor, constant: -5).isActive = true
        
        
    }
    
    override var isHighlighted: Bool {
        
        didSet {
            
            
        }
        
    }
    
    override var isSelected: Bool {
        
        didSet {
            
            modeImage.tintColor = isSelected ? UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1) : UIColor.black
            
            modeTitle.textColor = isSelected ? UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1) : UIColor.gray
            
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
