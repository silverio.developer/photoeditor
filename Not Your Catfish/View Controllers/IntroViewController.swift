//
//  IntroViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/10/05.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    let logo: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "nycLogo")
        img.alpha = 0
        
        return img
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        
    }
    
    
    func setup(){
        
        view.backgroundColor = .white
        
        view.addSubview(logo)
        
        logo.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        logo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 275).isActive = true
        logo.widthAnchor.constraint(equalToConstant: 275).isActive = true
        
        callHome()
        
    }

    func callHome(){
        
        UIView.animate(withDuration: 3, animations: {
            
            self.logo.alpha = 1
            
        }) { (true) in
            
            self.view!.window!.rootViewController = ViewController()
            
        }
        
    }
    
}
