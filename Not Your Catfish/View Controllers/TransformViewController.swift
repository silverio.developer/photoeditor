//
//  TransformViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/08/31.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit
import CropViewController

class TransformViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {

    
    let context = CIContext(options: [CIContextOption.workingColorSpace: CGColorSpaceCreateDeviceRGB()])
    
    let modeImages = ["crop3", "orientation1", "perspective", "invert", "original"]
    let modeTitles = ["Crop", "Rotate", "Perspective", "Invert", "Original"]
    
    let amountSlider: UISlider = {
        
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 1
        slider.value = 0.7
        
        return slider
        
    }()
    
    var topView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var topShadowLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .lightGray
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var middleView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        
        
        return vw
        
    }()
    
    var bottomView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.zPosition = 3
        
        
        return vw
        
    }()
    
    var imageBackground: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleToFill
        img.layer.zPosition = -1
        
        return img
        
    }()
    
    var imageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleAspectFit
        img.layer.zPosition = -1
        
        
        return img
        
    }()
    
    var previousImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "backward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    lazy var previousBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(previousF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var forwardBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    var forwardImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "foward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    var nycImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "transform-1")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var menuCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCell.self, forCellWithReuseIdentifier: "MenuId")
        cv.backgroundColor = .clear
        
        return cv
        
    }()
    
    var compareView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.cornerRadius = 8
        vw.layer.borderColor = UIColor.black.cgColor
        vw.layer.borderWidth = 2
        
        
        return vw
        
    }()
    
    var compareImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "compare")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    let compareLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .white
        lbl.text = "Long Press To Compare"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 15)
        lbl.layer.masksToBounds = true
        lbl.layer.cornerRadius = 8
        lbl.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        lbl.layer.zPosition = -1
        
        return lbl
        
    }()
    
    lazy var saveCurrentImage: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveCurrentImageF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var cancelTranform: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelTransoformF), for: .touchUpInside)
        
        return btn
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setup()
    }
    
    func addGestureRecognizer(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        
        longPress.delegate = self
        
        imageView.addGestureRecognizer(longPress)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    
    
    lazy var balloonPickerView: BalloonPickerView = {
        
        let bPV = BalloonPickerView()
        bPV.translatesAutoresizingMaskIntoConstraints = false
        bPV.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        bPV.value = 0
        bPV.minimumValue = 0
        bPV.maximumValue = 360
        bPV.addTarget(self, action: #selector(balloonChanged), for: .valueChanged)
        bPV.addTarget(self, action: #selector(saveCurrentImageStateF), for: .touchUpInside)
        bPV.layer.zPosition = 10
        
        return bPV
        
    }()
    
    var sliderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.zPosition = 7
        
        return vw
        
    }()
    
    lazy var cancelSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var saveSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let sliderLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        lbl.text = "Rotate"
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir-Light", size: 18)
        
        return lbl
        
    }()
    
    //
    
    let perspectiveView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.alpha = 0
        
        return vw
        
    }()
    
    lazy var topLeft: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "topLeft")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        btn.alpha = 1
        btn.addTarget(self, action: #selector(topLeftF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var topRight: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "topRight")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = .black
        btn.alpha = 1
        btn.addTarget(self, action: #selector(topRightF), for: .touchUpInside)
        
        return btn
        
    }()
    
    
    lazy var bottomLeft: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "bottomLeft")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = .black
        btn.alpha = 1
        
        btn.addTarget(self, action: #selector(bottomLeftF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var bottomRight: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "bottomRight")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = .black
        btn.alpha = 1
        
        btn.addTarget(self, action: #selector(bottomRightF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let flipContainer: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.alpha = 0
        
        return vw
        
    }()
    
    lazy var flipHorizontalBtn: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "flipHorizontal")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = .black
        btn.alpha = 1
        
        btn.addTarget(self, action: #selector(flipHorizontalF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var flipVerticalBtn: UIButton = {
        
        let btn = UIButton()
        
        let img = UIImage(named: "flipVertical")?.withRenderingMode(.alwaysTemplate)
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        btn.setImage(img, for: .normal)
        
        btn.tintColor = .black
        btn.alpha = 1
        
        btn.addTarget(self, action: #selector(flipVerticalF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let flipHorizontalLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.text = "Flip H"
        
        return lbl
        
    }()
    
    let flipVerticalLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.text = "Flip V"
        
        return lbl
        
    }()
    
    let flipSeperator: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        
        return vw
        
    }()
    
    var flipTopConstraint: NSLayoutConstraint!
    
    //
    
    var widthSize: CGFloat!
    
    var heightConstraint: NSLayoutConstraint!
    
    var cropViewController: CropViewController!
    
    var topConstraint: NSLayoutConstraint!
    
    func addPanGestureRecognizer(){
        
        panGes = UIPanGestureRecognizer(target: self, action: #selector(topLeftF))
        
        panGes.delegate = self
        
        imageView.addGestureRecognizer(panGes)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var topLeftPanGes: UIPanGestureRecognizer!
    
    func addTopLeftPanGestureRecognizer(){
        
        topLeftPanGes = UIPanGestureRecognizer(target: self, action: #selector(topLeftF))
        
        topLeftPanGes.delegate = self
        
        topLeft.addGestureRecognizer(topLeftPanGes)
        
        topLeft.isUserInteractionEnabled = true
        
    }
    
    func setup(){
        
        widthSize = view.frame.width - 16
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                widthSize = view.frame.width / 1.30
                
            case 1334:
                print("iPhone 6/6S/7/8")
                widthSize = view.frame.width / 1.25
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                widthSize = view.frame.width / 1.25
                
            case 2436:
                print("iPhone X, XS")
                
            case 2688:
                print("iPhone XS Max")
                
            case 1792:
                print("iPhone XR")
                
            default:
                print("Unknown")
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            
            widthSize = view.frame.width / 1.9
            
        }
        
        addGestureRecognizer()
        
        
        view.backgroundColor = UIColor.init(red: 212/255, green: 213/255, blue: 214/255, alpha: 1)
        
        view.addSubview(topView)
        
        view.addSubview(imageBackground)
        view.addSubview(imageView)
        view.addSubview(previousBtn)
        view.addSubview(forwardBtn)
        view.addSubview(previousImage)
        view.addSubview(forwardImage)
        
        view.addSubview(bottomView)
        bottomView.addSubview(menuCV)
        
        
        menuCV.delegate = self
        menuCV.dataSource = self
        
        
        view.addSubview(compareLbl)
        
        
        
        view.addSubview(flipContainer)
        flipContainer.addSubview(flipHorizontalBtn)
        flipContainer.addSubview(flipVerticalBtn)
        flipContainer.addSubview(flipHorizontalLbl)
        flipContainer.addSubview(flipVerticalLbl)
        flipContainer.addSubview(flipSeperator)
        
        view.addSubview(perspectiveView)
        perspectiveView.addSubview(topLeft)
        perspectiveView.addSubview(topRight)
        perspectiveView.addSubview(bottomLeft)
        perspectiveView.addSubview(bottomRight)
        
        topView.addSubview(nycImageView)
        
        topView.addSubview(saveCurrentImage)
        topView.addSubview(cancelTranform)
        
        view.addSubview(sliderBackground)
        sliderBackground.addSubview(balloonPickerView)
        sliderBackground.addSubview(cancelSlider)
        sliderBackground.addSubview(saveSlider)
        sliderBackground.addSubview(sliderLbl)
        
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        sliderBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        sliderBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topConstraint = sliderBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 105)
        topConstraint.isActive = true
        sliderBackground.heightAnchor.constraint(equalToConstant: 105).isActive = true
        
        saveSlider.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        saveSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        cancelSlider.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 15).isActive = true
        cancelSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        sliderLbl.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -20).isActive = true
        sliderLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        sliderLbl.widthAnchor.constraint(equalToConstant: 155).isActive = true
        sliderLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        balloonPickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        balloonPickerView.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -55).isActive = true
        balloonPickerView.widthAnchor.constraint(equalToConstant: 325).isActive = true
        balloonPickerView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        let balloonView = BalloonView()
        balloonView.image = #imageLiteral(resourceName: "balloon")
        balloonPickerView.baloonView = balloonView
        
        flipContainer.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -4).isActive = true
        flipContainer.heightAnchor.constraint(equalToConstant: 105).isActive = true
        flipContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 4).isActive = true
        flipContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -4).isActive = true
        
        flipVerticalBtn.centerYAnchor.constraint(equalTo: flipContainer.centerYAnchor).isActive = true
        flipVerticalBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        flipVerticalBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        flipVerticalBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 55).isActive = true
        
        flipHorizontalBtn.centerYAnchor.constraint(equalTo: flipContainer.centerYAnchor).isActive = true
        flipHorizontalBtn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        flipHorizontalBtn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        flipHorizontalBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -55).isActive = true
        
        flipVerticalLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        flipVerticalLbl.widthAnchor.constraint(equalToConstant: 95).isActive = true
        flipVerticalLbl.centerXAnchor.constraint(equalTo: flipVerticalBtn.centerXAnchor).isActive = true
        flipVerticalLbl.topAnchor.constraint(equalTo: flipVerticalBtn.bottomAnchor, constant: 8).isActive = true
        
        flipHorizontalLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        flipHorizontalLbl.widthAnchor.constraint(equalToConstant: 95).isActive = true
        flipHorizontalLbl.centerXAnchor.constraint(equalTo: flipHorizontalBtn.centerXAnchor).isActive = true
        flipHorizontalLbl.topAnchor.constraint(equalTo: flipHorizontalBtn.bottomAnchor, constant: 8).isActive = true
        
        flipSeperator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        flipSeperator.widthAnchor.constraint(equalToConstant: 2).isActive = true
        flipSeperator.centerYAnchor.constraint(equalTo: flipContainer.centerYAnchor).isActive = true
        flipSeperator.heightAnchor.constraint(equalToConstant: 75).isActive = true
        
        
        
        imageBackground.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        imageBackground.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -2).isActive = true
        imageBackground.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        imageBackground.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -2).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageBackground.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBackground.addSubview(blurEffectView)
        
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        
        
        
        forwardBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 35).isActive = true
        forwardBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        previousBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -35).isActive = true
        previousBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        forwardImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.centerXAnchor.constraint(equalTo: forwardBtn.centerXAnchor).isActive = true
        forwardImage.centerYAnchor.constraint(equalTo: forwardBtn.centerYAnchor).isActive = true
        
        previousImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.centerXAnchor.constraint(equalTo: previousBtn.centerXAnchor).isActive = true
        previousImage.centerYAnchor.constraint(equalTo: previousBtn.centerYAnchor).isActive = true
        
        
        compareLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        compareLbl.widthAnchor.constraint(equalToConstant: 195).isActive = true
        compareLbl.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -25).isActive = true
        compareLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
        nycImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nycImageView.widthAnchor.constraint(equalToConstant: view.frame.width - 55).isActive = true
        nycImageView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 40).isActive = true
        nycImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        saveCurrentImage.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -15).isActive = true
        saveCurrentImage.heightAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.widthAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        cancelTranform.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 15).isActive = true
        cancelTranform.heightAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.widthAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        updateImageConstraints()
        
        perspectiveView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -4).isActive = true
        perspectiveView.heightAnchor.constraint(equalToConstant: 105).isActive = true
        perspectiveView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 4).isActive = true
        perspectiveView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -4).isActive = true
        
        topLeft.heightAnchor.constraint(equalToConstant: 35).isActive = true
        topLeft.widthAnchor.constraint(equalToConstant: 35).isActive = true
        topLeft.leftAnchor.constraint(equalTo: imageView.leftAnchor, constant: 15).isActive = true
        topLeft.centerYAnchor.constraint(equalTo: perspectiveView.centerYAnchor).isActive = true
        
        topRight.heightAnchor.constraint(equalToConstant: 35).isActive = true
        topRight.widthAnchor.constraint(equalToConstant: 35).isActive = true
        topRight.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: -15).isActive = true
        topRight.centerYAnchor.constraint(equalTo: perspectiveView.centerYAnchor).isActive = true
        
        bottomLeft.heightAnchor.constraint(equalToConstant: 35).isActive = true
        bottomLeft.widthAnchor.constraint(equalToConstant: 35).isActive = true
        bottomLeft.leftAnchor.constraint(equalTo: topLeft.rightAnchor, constant: view.frame.width / 6).isActive = true
        bottomLeft.centerYAnchor.constraint(equalTo: perspectiveView.centerYAnchor).isActive = true
        
        bottomRight.heightAnchor.constraint(equalToConstant: 35).isActive = true
        bottomRight.widthAnchor.constraint(equalToConstant: 35).isActive = true
        bottomRight.rightAnchor.constraint(equalTo: topRight.leftAnchor, constant: -view.frame.width / 6).isActive = true
        bottomRight.centerYAnchor.constraint(equalTo: perspectiveView.centerYAnchor).isActive = true
        
        bottomView.bottomAnchor.constraint(equalTo: sliderBackground.topAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: view.frame.width / 5 + 25).isActive = true
        
        menuCV.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 15).isActive = true
        menuCV.heightAnchor.constraint(equalToConstant: view.frame.width / 5).isActive = true
        menuCV.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        menuCV.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
       
        
    }
    
    @objc func saveCurrentImageStateF(){
        
        drawOnImage()
        
        let newImage = self.imageView.image
        let imageData: NSData = newImage!.pngData()! as NSData
        
        //
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            let newImageNumber = imageNumber + 1
            print("saved !")
            UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
            UserDefaults.standard.set(newImageNumber, forKey: "maxImageNumber")
            UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
            
            
        }
        
        self.updateImageConstraints()
        
    }
    
    @objc func previousF(){
        
        //
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            if imageNumber != 1 {
                
                let newImageNumber = imageNumber - 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "image\(newImageNumber)") as? NSData {
                    
                    UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                    
                    imageView.image = UIImage(data: imageData as Data)
                    
                }
                
            }
            
        }
        
        updateImageConstraints()
    }
    
    @objc func forwardF(){
        
        print("ayt")
        
        if let max = UserDefaults.standard.object(forKey: "maxImageNumber") as? Int {
            
            print("maxxed:")
            print(max)
            
            if let current = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                print("currented:")
                print(current)
                
                if max >= current + 1 {
                    
                    print("through")
                    
                    let newCurrent = current + 1
                    
                    UserDefaults.standard.set(newCurrent, forKey: "currentImageNumber")
                    
                    if let imageData = UserDefaults.standard.object(forKey: "image\(newCurrent)") as? NSData {
                        
                        UserDefaults.standard.set(imageData, forKey: "image\(newCurrent)")
                        
                        imageView.image = UIImage(data: imageData as Data)
                        
                        self.updateImageConstraints()
                        
                    }
                    
                    
                }
                
            }
            
        }
        
    }
    
    @objc func cancelSliderF(){
        
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        removeSlider()
        
    }
    
    @objc func saveSliderF(){
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        removeSlider()
        
    }
    
    var flippedHorizontal = false
    var flippedVertical = false
    
    @objc func flipHorizontalF(){
        
        flippedVertical = false
        
        flipHorizontalBtn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        flipVerticalBtn.tintColor = .black
        
        flipHorizontalLbl.textColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        flipVerticalLbl.textColor = .black
        
        if flippedHorizontal == false {
            
            let newImage = UIImage(cgImage: (imageView.image?.cgImage!)!, scale: imageView.image!.scale, orientation: .downMirrored)
            
            imageView.image = newImage
            
            flippedHorizontal = true
        }else {
            
            let newImage = UIImage(cgImage: (imageView.image?.cgImage!)!, scale: imageView.image!.scale, orientation: .up)
            
            imageView.image = newImage
            
            flippedHorizontal = false
        }
        
        saveCurrentImageStateF()
        
    }
    
    @objc func flipVerticalF(){
        
        flippedHorizontal = false
        
        flipVerticalBtn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        flipHorizontalBtn.tintColor = .black
        
        flipVerticalLbl.textColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        flipHorizontalLbl.textColor = .black
        
        if flippedVertical == false {
            
            let newImage = UIImage(cgImage: (imageView.image?.cgImage!)!, scale: imageView.image!.scale, orientation: .upMirrored)
            
            imageView.image = newImage
            
            flippedVertical = true
        }else {
            
            let newImage = UIImage(cgImage: (imageView.image?.cgImage!)!, scale: imageView.image!.scale, orientation: .up)
            
            imageView.image = newImage
            
            flippedVertical = false
            
        }
        
        saveCurrentImageStateF()
        
    }
    
    var imageRotated = false
    var beginnerImage: UIImage!
    
    @objc func balloonChanged(){
        
        if imageRotated == false {
            
            beginnerImage = imageView.image
            
            imageRotated = true
        }
        
        imageView.image = rotateImage(image: beginnerImage, angle: CGFloat(balloonPickerView.value / 100), flipVertical: 0, flipHorizontal: 0)
        
        updateImageConstraints()
        
    }

    
    func rotateImage(image:UIImage, angle:CGFloat, flipVertical:CGFloat, flipHorizontal:CGFloat) -> UIImage? {
        
        
        let ciImage = CIImage(image: image)
        
        let filter = CIFilter(name: "CIAffineTransform")
        filter?.setValue(ciImage, forKey: kCIInputImageKey)
        filter?.setDefaults()
        
        let newAngle = angle * CGFloat(-1)
        
        var transform = CATransform3DIdentity
        transform = CATransform3DRotate(transform, CGFloat(newAngle), 0, 0, 1)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipVertical) * Double.pi), 0, 1, 0)
        transform = CATransform3DRotate(transform, CGFloat(Double(flipHorizontal) * Double.pi), 1, 0, 0)
        
        let affineTransform = CATransform3DGetAffineTransform(transform)
        
        filter?.setValue(NSValue(cgAffineTransform: affineTransform), forKey: "inputTransform")
        
        let contex = CIContext(options: [CIContextOption.useSoftwareRenderer:true])
        
        let outputImage = filter?.outputImage
        let cgImage = contex.createCGImage(outputImage!, from: (outputImage?.extent)!)
        
        let result = UIImage(cgImage: cgImage!)
        return result
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    
   
    
    func presentCropper(){
        
        cropViewController = CropViewController(image: imageView.image!)
        cropViewController.delegate = self
        
        
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            
            self.imageView.image = image
            
            let newImage = self.imageView.image
            let imageData: NSData = newImage!.pngData()! as NSData
            
            //
            
            if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                let newImageNumber = imageNumber + 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                UserDefaults.standard.set(newImageNumber, forKey: "maxImageNumber")
                UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                
                
            }
            
            self.updateImageConstraints()
            
        }
        
    }
    
    var newHeight: CGFloat!
    
    @objc func updateImageConstraints(){
        
        self.heightConstraint.isActive = false
        
        let ratio = self.imageView.image!.size.width / self.imageView.image!.size.height
        
        newHeight = self.widthSize / ratio
        self.heightConstraint.constant = newHeight
        self.heightConstraint.isActive = true
        
        self.view.layoutIfNeeded()
    }
    
    @objc func cancelTransoformF(){
        
        //cancel transforming image
        
        if let number = UserDefaults.standard.object(forKey: "firstOpenNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "currentImageNumber")
            
            if let data = UserDefaults.standard.object(forKey: "firstOpen") as? NSData {
                
                UserDefaults.standard.set(data, forKey: "image\(number)")
                
            }
            
        }
        
        dismiss(animated: true, completion: nil)
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Image saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    let imagePickerController = UIImagePickerController()
    
    @objc func saveCurrentImageF(){
        
        //save current state image
        
        dismiss(animated: true, completion: nil)
        
        /*imagePickerController.delegate = self
        imagePickerController.allowsEditing = false
        
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)*/
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            
            return self.imagePickerControllerDidCancel(picker)
            
        }
        
        self.imageView.image = image
        //peopleApi()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        picker.delegate = nil
    }
    
    var longPressed = false
    
    var currentImageDisplayed: UIImage!
    
    @objc func handleLongPress(){
        
        if longPressed == false {
            
            currentImageDisplayed = imageView.image
            
            if let originalImage = UserDefaults.standard.object(forKey: "image1") as? NSData {
                
                self.imageView.image = UIImage(data: originalImage as Data)
                
                self.updateImageConstraints()
                
            }
            
            longPressed = true
            
            
        }else {
            
            imageView.image = currentImageDisplayed
            
            self.updateImageConstraints()
            
            longPressed = false
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = menuCV.dequeueReusableCell(withReuseIdentifier: "MenuId", for: indexPath) as! MenuCell
        
        cell.modeImage.image = UIImage(named: modeImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        //cell.modeImage.tintColor = .black
        //cell.modeTitle.textColor = .black
        
        
        cell.modeTitle.text = modeTitles[indexPath.row]
        
        return cell
        
    }
    
    var panGes: UIPanGestureRecognizer!
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            imageRotated = false
            removeSlider()
            removePerspectiveButtons()
            presentCropper()
            removeFlipContainer()
            
        }else if indexPath.row == 1 {
            
            imageRotated = false
            
            removePerspectiveButtons()
            removeFlipContainer()
            
            topConstraint.isActive = false
            topConstraint.constant = -2
            topConstraint.isActive = true
            
            UIView.animate(withDuration: 0.7) {
                
                self.view.layoutIfNeeded()
                
            }
            
        }else if indexPath.row == 2 {
            
            imageRotated = false
            removeSlider()
            topLeftF()
            //addPanGestureRecognizer()
            placePerspectiveButtons()
            removeFlipContainer()
            
        }else if indexPath.row == 3 {
            
            imageRotated = false
            placeFlipContainer()
            removeSlider()
            removePerspectiveButtons()
            
        }else if indexPath.row == 4 {
            
            imageRotated = false
            removeSlider()
            removePerspectiveButtons()
            removeFlipContainer()
            callAlert()
            
        }
        
    }
    
    func callAlert(){
        
        let alert = UIAlertController(title: "Original Image", message: "Remove all editing to original image ? This action is not reversable.", preferredStyle: .alert)
        
        let okay = UIAlertAction(title: "Okay", style: .default) { (action) in
            
            
            
            //
            
            if let imageNumber = UserDefaults.standard.object(forKey: "firstOpenNumber") as? Int {
                
                
                UserDefaults.standard.set(imageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "firstOpen") as? NSData {
                    
                    self.imageView.image = UIImage(data: imageData as Data)
                    UserDefaults.standard.set(imageData, forKey: "image\(imageNumber)")
                    
                }
                
                
                
                
            }
            
            self.updateImageConstraints()
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        alert.addAction(okay)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func removeSlider(){
        
        topConstraint.isActive = false
        topConstraint.constant = 105
        topConstraint.isActive = true
        
        UIView.animate(withDuration: 0.7) {
            
            self.view.layoutIfNeeded()
            
        }
        
        
    }
    
    @objc func placePerspectiveButtons(){
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.perspectiveView.alpha = 1
            
        }
        
    }
    
    func placeFlipContainer(){
        
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.flipContainer.alpha = 1
            
        }
        
    }
    
    func removeFlipContainer(){
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.flipContainer.alpha = 0
            
        }
        
    }
    
    var imageToPerspect: UIImage!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        imageToPerspect = imageView.image!
        
    }
    
    var touchDidMove = false
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            
            
            if onTopLeft == true {
                
                
                let scaleFactor = UIScreen.main.scale
                let location = touch.location(in: self.imageView)
                
                let x = location.x * scaleFactor
                let y = newHeight * scaleFactor - location.y * scaleFactor

                touchDidMove = true
                
                let ciImage = CIImage(image: imageToPerspect)
                
                DispatchQueue.global(qos: .default).async {
                    
                    // Do heavy work here
                    
                    let perspectiveTransform = CIFilter(name: "CIPerspectiveTransform")!
                    
                    
                    
                    perspectiveTransform.setValue(CIVector(x: x, y: y),forKey: "inputTopLeft")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 510)),forKey: "inputTopRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 140)), forKey: "inputBottomRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 140)), forKey: "inputBottomLeft")
                    
                    
                    
                    perspectiveTransform.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        self!.imageView.image = UIImage(ciImage: perspectiveTransform.outputImage!)
                        self!.drawOnImage()
                        
                        
                    }
                }
                
                
                
            }
            
            if onTopRight == true {
                
                let scaleFactor = UIScreen.main.scale
                
                let location = touch.location(in: self.imageView)
                
                let x = location.x * scaleFactor
                let y = self.imageView.frame.height * scaleFactor - location.y * scaleFactor
                
                touchDidMove = true
                
                let ciImage = CIImage(image: imageToPerspect)
                
                DispatchQueue.global(qos: .default).async {
                    
                    // Do heavy work here
                    
                    let perspectiveTransform = CIFilter(name: "CIPerspectiveTransform")!
                    
                    
                    
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 510)),forKey: "inputTopLeft")
                    perspectiveTransform.setValue(CIVector(x: x, y: y),forKey: "inputTopRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 140)), forKey: "inputBottomRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 140)), forKey: "inputBottomLeft")
                    
                    
                    perspectiveTransform.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        self!.imageView.image = UIImage(ciImage: perspectiveTransform.outputImage!)
                        self!.drawOnImage()
                        
                    }
                }
                
            }
            if onBottomLeft == true {
                
                let scaleFactor = UIScreen.main.scale
                
                let location = touch.location(in: self.imageView)
                
                let x = location.x * scaleFactor
                let y = self.imageView.bounds.height * scaleFactor - location.y * scaleFactor
                
                touchDidMove = true
                
                let ciImage = CIImage(image: imageToPerspect)
                
                DispatchQueue.global(qos: .default).async {
                    
                    // Do heavy work here
                    
                    let perspectiveTransform = CIFilter(name: "CIPerspectiveTransform")!
                    
                    
                    
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 510)),forKey: "inputTopLeft")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 510)),forKey: "inputTopRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 140)), forKey: "inputBottomRight")
                    perspectiveTransform.setValue(CIVector(x: x, y: y),forKey: "inputBottomLeft")
                    
                    
                    perspectiveTransform.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        self!.imageView.image = UIImage(ciImage: perspectiveTransform.outputImage!)
                        self!.drawOnImage()
                        
                    }
                }
                
            }
            
            if onBottomRight == true {
                
                let scaleFactor = UIScreen.main.scale
                
                let location = touch.location(in: self.imageView)
                
                let x = location.x * scaleFactor
                let y = self.imageView.bounds.height * scaleFactor - location.y * scaleFactor
                
                touchDidMove = true
                
                let ciImage = CIImage(image: imageToPerspect)
                
                DispatchQueue.global(qos: .default).async {
                    
                    // Do heavy work here
                    
                    let perspectiveTransform = CIFilter(name: "CIPerspectiveTransform")!
                    
                    
                    
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 510)),forKey: "inputTopLeft")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 650, y: 510)),forKey: "inputTopRight")
                    perspectiveTransform.setValue(CIVector(x: x, y: y),forKey: "inputBottomRight")
                    perspectiveTransform.setValue(CIVector(cgPoint: CGPoint(x: 150, y: 140)), forKey: "inputBottomLeft")
                    
                    
                    perspectiveTransform.setValue(ciImage, forKey: kCIInputImageKey)
                    
                    
                    DispatchQueue.main.async { [weak self] in
                        
                        self!.imageView.image = UIImage(ciImage: perspectiveTransform.outputImage!)
                        self!.drawOnImage()
                        
                    }
                }
                
            }
            
            
            
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if touchDidMove == true {
            
            let newImage = self.imageView.image
            let imageData: NSData = newImage!.pngData()! as NSData
            
            //
            
            if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                let newImageNumber = imageNumber + 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                
                
            }
            
            self.updateImageConstraints()
            
            touchDidMove = false
            
        }
        
    }
    
    func drawOnImage(){
        
        let hasAlpha = true
        let scale: CGFloat = UIScreen.main.scale // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(imageView.frame.size, !hasAlpha, scale)
        
        imageView.image!.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
        
        UIGraphicsGetCurrentContext()?.saveGState()
        UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
        
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsGetCurrentContext()?.restoreGState()
        UIGraphicsEndImageContext()
    }
    
    @objc func topLeftF(){
        
       
        onTopLeft = true
        onTopRight = false
        onBottomLeft = false
        onBottomRight = false
        
        topRight.tintColor = .black
        topLeft.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        bottomLeft.tintColor = .black
        bottomRight.tintColor = .black
        
    }
    
    
    @objc func topRightF(){
        
        onTopLeft = false
        onTopRight = true
        onBottomLeft = false
        onBottomRight = false
        
        topRight.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        topLeft.tintColor = .black
        bottomLeft.tintColor = .black
        bottomRight.tintColor = .black
        
    }
    
    @objc func bottomLeftF(){
        
        onTopLeft = false
        onTopRight = false
        onBottomLeft = true
        onBottomRight = false
        
        topRight.tintColor = .black
        topLeft.tintColor = .black
        bottomLeft.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        bottomRight.tintColor = .black
        
    }
    
    @objc func bottomRightF(){
        
        onTopLeft = false
        onTopRight = false
        onBottomLeft = false
        onBottomRight = true
        
        topRight.tintColor = .black
        topLeft.tintColor = .black
        bottomLeft.tintColor = .black
        bottomRight.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
    }
    
    var onTopLeft = false
    var onTopRight = false
    var onBottomLeft = false
    var onBottomRight = false
    
    
    @objc func removePerspectiveButtons(){
        
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.perspectiveView.alpha = 0
            
        }
        
    }
    
    @objc func callTransform(){
        
        let vC = TransformViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callBeautify(){
        
        let vC = BeautifyViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    @objc func callMask(){
        
        let vC = MaskViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callFilter(){
        
        let vC = FilterViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: view.frame.width / 5, height: view.frame.width / 5)
        
        return size
        
    }
    
    
    
    
}
