//
//  BeautifyViewController.swift
//  Not Your Catfish
//
//  Created by Thapelo on 2019/08/31.
//  Copyright © 2019 Thapelo. All rights reserved.
//

import UIKit
import CropViewController
import ColorSlider
import Fritz
import YUCIHighPassSkinSmoothing
import NVActivityIndicatorView
import Lottie

class BeautifyViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    let context = CIContext(options: [CIContextOption.workingColorSpace: CGColorSpaceCreateDeviceRGB()])
    let filter = YUCIHighPassSkinSmoothing()
    
    let modeImages = ["make-up", "hair-cut", "tap2", "hula-hoop", "original"]
    let modeTitles = ["Smoother", "Hair Color", "Lighten", "Mask", "Original"]
    
    let amountSlider: UISlider = {
        
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.maximumValue = 1
        slider.value = 0.7
        
        return slider
        
    }()
    
    var topView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var topShadowLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .lightGray
        vw.layer.masksToBounds = true
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 100, height: 25)
        
        return vw
        
    }()
    
    var middleView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        
        
        return vw
        
    }()
    
    var bottomView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        
        return vw
        
    }()
    
    var imageBackground: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        //img.backgroundColor = .yellow
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleToFill
        //img.image = UIImage(named: "reach")
        
        
        return img
        
    }()
    
    var maskImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var imageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "afro")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    
    var lightenImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.contentMode = .scaleAspectFill
        img.image = UIImage(named: "transparentBlack")
        img.alpha = 0
        
        return img
        
    }()
    
    var previousImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "backward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    lazy var previousBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(previousF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var forwardBtn: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = UIColor.init(red: 59/255, green: 56/255, blue: 54/255, alpha: 0.8)
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    var forwardImage: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .clear
        img.image = UIImage(named: "foward")?.withRenderingMode(.alwaysTemplate)
        img.contentMode = .scaleAspectFit
        img.tintColor = .white
        
        
        return img
        
    }()
    
    var nycImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "beautifyLogo")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    var menuCV: UICollectionView = {
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCell.self, forCellWithReuseIdentifier: "MenuId")
        cv.backgroundColor = .clear
        
        return cv
        
    }()
    
    var compareView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.masksToBounds = true
        vw.layer.cornerRadius = 8
        vw.layer.borderColor = UIColor.black.cgColor
        vw.layer.borderWidth = 2
        
        
        return vw
        
    }()
    
    var compareImageView: UIImageView = {
        
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.backgroundColor = .white
        img.image = UIImage(named: "compare")
        img.contentMode = .scaleAspectFit
        
        
        return img
        
    }()
    
    let compareLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = NSTextAlignment.center
        lbl.textColor = .white
        lbl.text = "Long Press To Compare"
        lbl.font = UIFont(name: "AvenirNext-Regular", size: 15)
        lbl.layer.masksToBounds = true
        lbl.layer.cornerRadius = 8
        lbl.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        return lbl
        
    }()
    
    lazy var saveCurrentImage: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveCurrentImageF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var cancelTranform: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelTransoformF), for: .touchUpInside)
        
        return btn
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    
    
    func addGestureRecognizer(){
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        
        longPress.delegate = self
        
        imageView.addGestureRecognizer(longPress)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    var heightConstraint: NSLayoutConstraint!
    
    var cropViewController: CropViewController!
    
    lazy var balloonPickerView: BalloonPickerView = {
        
        let bPV = BalloonPickerView()
        bPV.translatesAutoresizingMaskIntoConstraints = false
        bPV.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        bPV.value = 70
        bPV.addTarget(self, action: #selector(balloonChanged), for: .touchUpInside)
        bPV.addTarget(self, action: #selector(lightenSizeF), for: .valueChanged)
        bPV.alpha = 1
        
        
        return bPV
        
    }()
    
    var sliderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        
        return vw
        
    }()
    
    lazy var cancelSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(cancelSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var saveSlider: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        let img = UIImage(named: "tick")?.withRenderingMode(.alwaysTemplate)
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.init(red: 0/255, green: 180/255, blue: 214/255, alpha: 1)
        
        btn.addTarget(self, action: #selector(saveSliderF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let sliderLbl: UILabel = {
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        lbl.text = "Smoother"
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = UIFont(name: "Avenir-Light", size: 18)
        
        return lbl
        
    }()
    
    lazy var timesOne: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 20
        btn.setTitle("x 1", for: .normal)
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.setTitleColor(UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1), for: .normal)
        btn.layer.borderWidth = 2
        
        btn.addTarget(self, action: #selector(timesOneF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var timesOneSelector: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 24
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.layer.borderWidth = 2
        
      //  btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var timesTwo: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 25
        btn.setTitle("x 2", for: .normal)
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.setTitleColor(UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1), for: .normal)
        btn.layer.borderWidth = 2
        
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var timesTwoSelector: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 29
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.layer.borderWidth = 2
        btn.alpha = 0
        
        btn.addTarget(self, action: #selector(timesTwoF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var timesThree: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 30
        btn.setTitle("x 3", for: .normal)
        btn.setTitleColor(UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1), for: .normal)
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.layer.borderWidth = 2
        
        btn.addTarget(self, action: #selector(forwardF), for: .touchUpInside)
        
        return btn
        
    }()
    
    lazy var timesThreeSelector: UIButton = {
        
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.masksToBounds = true
        btn.layer.cornerRadius = 34
        btn.layer.borderColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1).cgColor
        btn.layer.borderWidth = 2
        btn.alpha = 0
        
        btn.addTarget(self, action: #selector(timesThreeF), for: .touchUpInside)
        
        return btn
        
    }()
    
    let firstLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        return vw
        
    }()
    
    let secondLine: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        
        return vw
        
    }()
    
    let timesHolder: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .clear
        vw.alpha = 0
        
        return vw
        
    }()
    
    var loaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoaderBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .white
        vw.layer.cornerRadius = 18
        vw.layer.masksToBounds = true
        vw.alpha = 0
        vw.layer.zPosition = 5
        
        return vw
        
    }()
    
    var loadingLoader: NVActivityIndicatorView = {
        
        let loader = NVActivityIndicatorView(frame: .zero)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.type = .ballTrianglePath
        loader.tintColor = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.color = UIColor.init(red: 255/255, green: 1/255, blue: 73/255, alpha: 1)
        loader.layer.zPosition = 5
        
        return loader
        
    }()
    
    var lotAnimationView: LottieView = {
        
        let vw = LottieView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        
        
        return vw
        
    }()
    
    var animationView: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        
        
        return vw
        
    }()
    
    var anView: AnimationView = {
        
        let vw = AnimationView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        
        vw.animation = Animation.filepath("/Users/thapelo/Downloads/Not Your Catfish/Not Your Catfish/2478-check.json")
        vw.loopMode = LottieLoopMode.repeat(10)
        
        return vw
        
    }()
    
    var lightenSizeBackground: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .black
        vw.alpha = 0
        vw.layer.cornerRadius = 12
        vw.layer.masksToBounds = true
        
        return vw
        
    }()
    
    var lightenSizeWheel: UIView = {
        
        let vw = UIView()
        vw.translatesAutoresizingMaskIntoConstraints = false
        vw.backgroundColor = .clear
        vw.layer.borderColor = UIColor.white.cgColor
        vw.layer.borderWidth = 2
        vw.layer.cornerRadius = 20
        vw.layer.masksToBounds = true
        
        
        return vw
        
    }()
    
    private lazy var visionModel = FritzVisionHairSegmentationModelAccurate()
    
    var originalImage: UIImage!
    var changedImage: UIImage!
    
    var topConstraint: NSLayoutConstraint!
    
    var rightConstraint: NSLayoutConstraint!
    
    var bottomConstraint: NSLayoutConstraint!
    
    var lightenSizeWidth: NSLayoutConstraint!
    var lightenSizeHeight: NSLayoutConstraint!
    
    var maskHeightConstraint: NSLayoutConstraint!
    
    var lightenHeightConstraint: NSLayoutConstraint!
    
    var imageViewScale: CGFloat!
    var imageViewOrientation: UIImage.Orientation!
    
    var imageToSmooth: UIImage!
    
    var firstMaskImage: UIImage!
    
    func setup(){
        
        originalImage = imageView.image!
        
        var widthSize = view.frame.width - 16
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                widthSize = view.frame.width / 1.30
                
            case 1334:
                print("iPhone 6/6S/7/8")
                widthSize = view.frame.width / 1.25
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                widthSize = view.frame.width / 1.25
                
            case 2436:
                print("iPhone X, XS")
                
            case 2688:
                print("iPhone XS Max")
                
            case 1792:
                print("iPhone XR")
                
            default:
                print("Unknown")
            }
        } else if UIDevice().userInterfaceIdiom == .pad {
            
            widthSize = view.frame.width / 1.9
            
        }
        
        addGestureRecognizer()
        
        
        view.backgroundColor = UIColor.init(red: 212/255, green: 213/255, blue: 214/255, alpha: 1)
        
        view.addSubview(topView)
        
        view.addSubview(imageBackground)
        
        view.addSubview(maskImageView)
        view.addSubview(imageView)
        view.addSubview(lightenImageView)
        view.addSubview(previousBtn)
        view.addSubview(forwardBtn)
        view.addSubview(previousImage)
        view.addSubview(forwardImage)
        
        view.addSubview(loaderBackground)
        view.addSubview(lightenSizeBackground)
        lightenSizeBackground.addSubview(lightenSizeWheel)
        
        
        
        menuCV.delegate = self
        menuCV.dataSource = self
        
        //view.addSubview(compareView)
        //compareView.addSubview(compareImageView)
        view.addSubview(compareLbl)
        
        view.addSubview(bottomView)
        bottomView.addSubview(menuCV)
        
        view.addSubview(sliderBackground)
        sliderBackground.addSubview(timesHolder)
        sliderBackground.addSubview(balloonPickerView)
        sliderBackground.addSubview(cancelSlider)
        sliderBackground.addSubview(saveSlider)
        sliderBackground.addSubview(sliderLbl)
        
        timesHolder.addSubview(timesOne)
        timesHolder.addSubview(timesTwo)
        timesHolder.addSubview(timesThree)
        
        timesHolder.addSubview(timesOneSelector)
        timesHolder.addSubview(timesTwoSelector)
        timesHolder.addSubview(timesThreeSelector)
        
        timesHolder.addSubview(firstLine)
        timesHolder.addSubview(secondLine)
        
        topView.addSubview(nycImageView)
        
        topView.addSubview(saveCurrentImage)
        topView.addSubview(cancelTranform)
        
        view.addSubview(loadingLoaderBackground)
        loadingLoaderBackground.addSubview(loadingLoader)
        view.addSubview(lotAnimationView)
        view.addSubview(anView)
        
        
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        sliderBackground.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        sliderBackground.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        topConstraint = sliderBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        topConstraint.isActive = true
        sliderBackground.heightAnchor.constraint(equalToConstant: 105).isActive = true
        
        saveSlider.rightAnchor.constraint(equalTo: bottomView.rightAnchor, constant: -15).isActive = true
        saveSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        saveSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        cancelSlider.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: 15).isActive = true
        cancelSlider.heightAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.widthAnchor.constraint(equalToConstant: 20).isActive = true
        cancelSlider.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -28).isActive = true
        
        sliderLbl.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -20).isActive = true
        sliderLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        sliderLbl.widthAnchor.constraint(equalToConstant: 155).isActive = true
        sliderLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        balloonPickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        balloonPickerView.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -55).isActive = true
        balloonPickerView.widthAnchor.constraint(equalToConstant: 325).isActive = true
        balloonPickerView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
       /* bottomConstraint = timesTwo.bottomAnchor.constraint(equalTo: sliderBackground.bottomAnchor, constant: -55)
        bottomConstraint.isActive = true
        timesTwo.heightAnchor.constraint(equalToConstant: 50).isActive = true
        timesTwo.widthAnchor.constraint(equalToConstant: 50).isActive = true
        timesTwo.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        timesOne.centerYAnchor.constraint(equalTo: timesTwo.centerYAnchor).isActive = true
        timesOne.heightAnchor.constraint(equalToConstant: 40).isActive = true
        timesOne.widthAnchor.constraint(equalToConstant: 40).isActive = true
        timesOne.rightAnchor.constraint(equalTo: timesTwo.leftAnchor, constant: -75).isActive = true
        
        timesThree.centerYAnchor.constraint(equalTo: timesTwo.centerYAnchor).isActive = true
        timesThree.heightAnchor.constraint(equalToConstant: 60).isActive = true
        timesThree.widthAnchor.constraint(equalToConstant: 60).isActive = true
        timesThree.leftAnchor.constraint(equalTo: timesTwo.rightAnchor, constant: 75).isActive = true
        
        timesTwoSelector.centerYAnchor.constraint(equalTo: timesTwo.centerYAnchor).isActive = true
        timesTwoSelector.heightAnchor.constraint(equalToConstant: 58).isActive = true
        timesTwoSelector.widthAnchor.constraint(equalToConstant: 58).isActive = true
        timesTwoSelector.centerXAnchor.constraint(equalTo: timesTwo.centerXAnchor).isActive = true
        
        timesOneSelector.centerYAnchor.constraint(equalTo: timesOne.centerYAnchor).isActive = true
        timesOneSelector.heightAnchor.constraint(equalToConstant: 48).isActive = true
        timesOneSelector.widthAnchor.constraint(equalToConstant: 48).isActive = true
        timesOneSelector.centerXAnchor.constraint(equalTo: timesOne.centerXAnchor).isActive = true
        
        timesThreeSelector.centerYAnchor.constraint(equalTo: timesThree.centerYAnchor).isActive = true
        timesThreeSelector.heightAnchor.constraint(equalToConstant: 68).isActive = true
        timesThreeSelector.widthAnchor.constraint(equalToConstant: 68).isActive = true
        timesThreeSelector.centerXAnchor.constraint(equalTo: timesThree.centerXAnchor).isActive = true
        
        firstLine.heightAnchor.constraint(equalToConstant: 2).isActive = true
        firstLine.centerYAnchor.constraint(equalTo: timesTwo.centerYAnchor).isActive = true
        firstLine.leftAnchor.constraint(equalTo: timesOne.rightAnchor, constant: 8).isActive = true
        firstLine.rightAnchor.constraint(equalTo: timesTwo.leftAnchor, constant: -8).isActive = true
        
        secondLine.heightAnchor.constraint(equalToConstant: 2).isActive = true
        secondLine.centerYAnchor.constraint(equalTo: timesTwo.centerYAnchor).isActive = true
        secondLine.leftAnchor.constraint(equalTo: timesTwo.rightAnchor, constant: 8).isActive = true
        secondLine.rightAnchor.constraint(equalTo: timesThree.leftAnchor, constant: -8).isActive = true*/
        
        let balloonView = BalloonView()
        balloonView.image = #imageLiteral(resourceName: "balloon")
        balloonPickerView.baloonView = balloonView
        
        
        
        
        
        imageBackground.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 2).isActive = true
        imageBackground.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -2).isActive = true
        imageBackground.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        imageBackground.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -2).isActive = true
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = imageBackground.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBackground.addSubview(blurEffectView)
        
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        
        maskImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        maskHeightConstraint = maskImageView.heightAnchor.constraint(equalToConstant: 0)
        maskHeightConstraint.isActive = true
        maskImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        maskImageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        maskImageView.alpha = 0
        
        lightenImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        lightenHeightConstraint = lightenImageView.heightAnchor.constraint(equalToConstant: 0)
        lightenHeightConstraint.isActive = true
        lightenImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lightenImageView.widthAnchor.constraint(equalToConstant: widthSize).isActive = true
        //imageView.frame.size.width = view.frame.size.width - 16
        //imageView.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: -8).isActive = true
        
        
        
        forwardBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        forwardBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 35).isActive = true
        forwardBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        previousBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        previousBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -35).isActive = true
        previousBtn.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 25).isActive = true
        
        forwardImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        forwardImage.centerXAnchor.constraint(equalTo: forwardBtn.centerXAnchor).isActive = true
        forwardImage.centerYAnchor.constraint(equalTo: forwardBtn.centerYAnchor).isActive = true
        
        previousImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        previousImage.centerXAnchor.constraint(equalTo: previousBtn.centerXAnchor).isActive = true
        previousImage.centerYAnchor.constraint(equalTo: previousBtn.centerYAnchor).isActive = true
        
       
        
        /*compareView.heightAnchor.constraint(equalToConstant: 60).isActive = true
         compareView.widthAnchor.constraint(equalToConstant:60).isActive = true
         compareView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 5).isActive = true
         compareView.topAnchor.constraint(equalTo: menuCV.bottomAnchor, constant: 5).isActive = true*/
        
        compareLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        compareLbl.widthAnchor.constraint(equalToConstant: 195).isActive = true
        compareLbl.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -25).isActive = true
        compareLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        
        nycImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nycImageView.widthAnchor.constraint(equalToConstant: view.frame.width - 55).isActive = true
        nycImageView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 40).isActive = true
        nycImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        saveCurrentImage.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -15).isActive = true
        saveCurrentImage.heightAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.widthAnchor.constraint(equalToConstant: 35).isActive = true
        saveCurrentImage.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        cancelTranform.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 15).isActive = true
        cancelTranform.heightAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.widthAnchor.constraint(equalToConstant: 35).isActive = true
        cancelTranform.topAnchor.constraint(equalTo: topView.topAnchor, constant: 50).isActive = true
        
        
        
        lotAnimationView.widthAnchor.constraint(equalToConstant: 55).isActive = true
        lotAnimationView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        lotAnimationView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -75).isActive = true
        lotAnimationView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        anView.widthAnchor.constraint(equalToConstant: 155).isActive = true
        anView.heightAnchor.constraint(equalToConstant: 155).isActive = true
        anView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 75).isActive = true
        anView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        lightenSizeBackground.heightAnchor.constraint(equalToConstant: 125).isActive = true
        lightenSizeBackground.widthAnchor.constraint(equalToConstant: 125).isActive = true
        lightenSizeBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lightenSizeBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        lightenSizeHeight = lightenSizeWheel.heightAnchor.constraint(equalToConstant: 40)
        lightenSizeHeight.isActive = true
        lightenSizeWidth = lightenSizeWheel.widthAnchor.constraint(equalToConstant: 40)
        lightenSizeWidth.isActive = true
        lightenSizeWheel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lightenSizeWheel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
       // let checkAnimation = AnimationView(name: "2478-check")
        
        
        //anView.play()
        
        
        
        
        /*compareImageView.leftAnchor.constraint(equalTo: compareView.leftAnchor).isActive = true
         compareImageView.rightAnchor.constraint(equalTo: compareView.rightAnchor).isActive = true
         compareImageView.topAnchor.constraint(equalTo: compareView.topAnchor).isActive = true
         compareImageView.bottomAnchor.constraint(equalTo: compareLbl.topAnchor).isActive = true*/
        
        menuCV.selectItem(at: [0,0], animated: true, scrollPosition: [])
        
        heightConstraint.isActive = false
        maskHeightConstraint.isActive = false
        lightenHeightConstraint.isActive = false
        
        let ratio = imageView.image!.size.width / imageView.image!.size.height
        
        
        let newHeight = widthSize / ratio
        heightConstraint.constant = newHeight
        heightConstraint.isActive = true
        
        maskHeightConstraint.constant = newHeight
        maskHeightConstraint.isActive = true
        
        lightenHeightConstraint.constant = newHeight
        lightenHeightConstraint.isActive = true
        
        view.layoutIfNeeded()
        
        view.addSubview(colorSlider)
        
        rightConstraint = colorSlider.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 75)
        rightConstraint.isActive = true
        colorSlider.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        colorSlider.heightAnchor.constraint(equalToConstant: view.frame.width / 2).isActive = true
        colorSlider.widthAnchor.constraint(equalToConstant: 15).isActive = true
        
        loaderBackground.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        loaderBackground.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        loaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoaderBackground.widthAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.heightAnchor.constraint(equalToConstant: 95).isActive = true
        loadingLoaderBackground.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoaderBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        loadingLoader.widthAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.heightAnchor.constraint(equalToConstant: 55).isActive = true
        loadingLoader.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loadingLoader.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        bottomView.bottomAnchor.constraint(equalTo: sliderBackground.topAnchor).isActive = true
        bottomView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        bottomView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: view.frame.width / 5 + 25).isActive = true
        
        menuCV.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 15).isActive = true
        menuCV.heightAnchor.constraint(equalToConstant: view.frame.width / 5).isActive = true
        menuCV.leftAnchor.constraint(equalTo: bottomView.leftAnchor).isActive = true
        menuCV.rightAnchor.constraint(equalTo: bottomView.rightAnchor).isActive = true
        
        colorSlider.addTarget(self, action: #selector(changedColor(_:)), for: .touchUpInside)
        
        perform(#selector(balloonChanged), with: nil, afterDelay: 0.7)
        
        imageViewScale = imageView.image!.scale
        imageViewOrientation = imageView.image!.imageOrientation
        
        imageToSmooth = imageView.image!
        
        firstMaskImage = imageView.image!
        
    }
    
    var shouldChangeHairColor = false
    
    @objc func cancelSliderF(){
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        if shouldChangeHairColor == true {
            
            removeColorSlider()
            
            shouldChangeHairColor = false
            
        }
        
        removeSlider()
        
    }
    
    @objc func saveSliderF(){
        
        
        
        menuCV.selectItem(at: [], animated: true, scrollPosition: [])
        
        removeSlider()
        
    }
    
    func removeSlider(){
        
        topConstraint.isActive = false
        topConstraint.constant = 105
        topConstraint.isActive = true
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    @objc func timesOneF(){
        
        timesOneSelector.alpha = 1
        timesTwoSelector.alpha = 0
        timesThreeSelector.alpha = 0
        
    }
    
    @objc func timesTwoF(){
        
        timesOneSelector.alpha = 0
        timesTwoSelector.alpha = 1
        timesThreeSelector.alpha = 0
    }
    
    @objc func timesThreeF(){
        timesOneSelector.alpha = 0
        timesTwoSelector.alpha = 0
        timesThreeSelector.alpha = 1
        
    }
    
    @objc func startCheckAnimation(){
        
        
        
    }
    
    var processedImage: UIImage?
    
    var image: UIImage? = nil {
        didSet {
            
            loadingLoaderBackground.alpha = 0
            loaderBackground.alpha = 0
            loadingLoader.stopAnimating()
            
        }
    }
    
    
    
    var isProcessing = false
    
    @objc func smoothOnly() {
        
        /*let newfilter = YUCIHighPassSkinSmoothing()
        newfilter.inputImage = ...
        newfilter.inputAmount = ...
        let outputImage = newfilter.outputImage!*/

        /* Or */
        
        inputCIImage = CIImage(image: imageView.image!)
        
        self.filter.inputImage = inputCIImage
        self.filter.inputAmount = balloonPickerView.value / Double(100) as NSNumber
        self.filter.inputRadius = 7.0 * inputCIImage!.extent.width/750.0 as NSNumber
        let outputCIImage = filter.outputImage!
        
        let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: imageViewScale, orientation: imageViewOrientation)
        
        self.processedImage = outputUIImage
        
        imageView.image = self.processedImage!
        
        
        
    }
    
    
    func processImage() {
        
        isProcessing = true
        
        
        
        
        print(self.filter.outputKeys)
        self.filter.inputImage = inputCIImage
        self.filter.inputAmount = balloonPickerView.value / Double(100) as NSNumber
        self.filter.inputRadius = 7.0 * inputCIImage!.extent.width/750.0 as NSNumber
        let outputCIImage = filter.outputImage!
        
        let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: imageViewScale, orientation: imageViewOrientation)
        
        self.processedImage = outputUIImage
        
        
    }
    
    @objc func callLoader(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
    }
    
    var inputCIImage: CIImage!
    
    
    
    @objc func balloonChanged(){
        
        imageFromMainThread = imageView.image!
        
        
        if onSmoother == true {
            
            loadingLoaderBackground.alpha = 1
            loadingLoader.startAnimating()
            loaderBackground.alpha = 0.7
            view.isUserInteractionEnabled = false
            
            inputCIImage = CIImage(image: imageToSmooth)
            
            DispatchQueue.global(qos: .default).async {
                
                // Do heavy work here
                self.processImage()
                
                DispatchQueue.main.async { [weak self] in
                    // UI updates must be on main thread
                    self!.imageView.image = self!.processedImage
                    self!.changedImage = self!.processedImage
                    self!.saveCurrentImageStateF()
                    self!.image = self!.processedImage
                    self!.view.isUserInteractionEnabled = true
                    
                }
            }
            
            
        }else if onLighten == true {
            
            
            
        }else if onMask == true {
            
            
            
        } else {
            
            loadingLoaderBackground.alpha = 1
            loadingLoader.startAnimating()
            loaderBackground.alpha = 0.7
            view.isUserInteractionEnabled = false
            
            DispatchQueue.global(qos: .default).async {
                
                // Do heavy work here
                self.changeHairColor()
                
                DispatchQueue.main.async { [weak self] in
                    // UI updates must be on main thread
                    self!.imageView.image = self!.blended
                    self!.saveCurrentImageStateF()
                    self!.loadingLoaderBackground.alpha = 0
                    self!.loadingLoader.stopAnimating()
                    self!.loaderBackground.alpha = 0
                    self!.view.isUserInteractionEnabled = true

                    self?.imageToSmooth = self!.imageView.image
                    
                    
                }
            }
            
            
        }
        
       
        
    }
    
    
    func saveCurrentImageStateF(){
        
        let newImage = self.imageView.image
        let imageData: NSData = newImage!.pngData()! as NSData
        
        //
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            let newImageNumber = imageNumber + 1
            
            UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
            UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
            UserDefaults.standard.set(newImageNumber, forKey: "maxImageNumber")
            
            
        } 
    }
    
    var lineWidth = 20.0
    
    @objc func lightenSizeF(){
        
        if onLighten == true || onMask == true {
            
            lightenSizeBackground.alpha = 0.7
            
            lightenSizeHeight.isActive = false
            lightenSizeWidth.isActive = false
            
            lightenSizeHeight.constant = CGFloat(balloonPickerView.value)
            lightenSizeWidth.constant = CGFloat(balloonPickerView.value)
            lightenSizeWheel.layer.cornerRadius = CGFloat(balloonPickerView.value / 2)
            
            
            lightenSizeHeight.isActive = true
            lightenSizeWidth.isActive = true
            
          //  lightenSizeWheel.frame.origin = CGPoint(x: lightenSizeBackground.frame.midX, y: lightenSizeBackground.frame.midY)
            lineWidth = balloonPickerView.value / 2
            
            perform(#selector(removeLightenSizeBackground), with: nil, afterDelay: 0.7)
            
        }
        
    }
    
    @objc func removeLightenSizeBackground(){
        
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.lightenSizeBackground.alpha = 0
            
        }
        
    }
    
    let colorSlider: ColorSlider = {
        
        let cS = ColorSlider()
        cS.translatesAutoresizingMaskIntoConstraints = false
        
        
        return cS
        
    }()
    
    @objc func changedColor(_ slider: ColorSlider) {
        var slidercolor = slider.color //uicolor
        maskColor = slidercolor
        balloonChanged()
        
        // ...
    }
    
    @objc func previousF(){
        
        if let imageNumber = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
            
            if imageNumber != 1 {
                
                let newImageNumber = imageNumber - 1
                
                UserDefaults.standard.set(newImageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "image\(newImageNumber)") as? NSData {
                    
                    UserDefaults.standard.set(imageData, forKey: "image\(newImageNumber)")
                    
                    imageView.image = UIImage(data: imageData as Data)
                    
                    self.imageToSmooth = imageView.image
                    
                }
                
            }
            
        }
        
    }
    
    @objc func forwardF(){
        
        print("ayt")
        
        if let max = UserDefaults.standard.object(forKey: "maxImageNumber") as? Int {
            
            print("maxxed:")
            print(max)
            
            if let current = UserDefaults.standard.object(forKey: "currentImageNumber") as? Int {
                
                print("currented:")
                print(current)
                
                if max >= current + 1 {
                    
                    print("through")
                    
                    let newCurrent = current + 1
                    
                    UserDefaults.standard.set(newCurrent, forKey: "currentImageNumber")
                    
                    if let imageData = UserDefaults.standard.object(forKey: "image\(newCurrent)") as? NSData {
                        
                        UserDefaults.standard.set(imageData, forKey: "image\(newCurrent)")
                        
                        imageView.image = UIImage(data: imageData as Data)
                        
                        self.imageToSmooth = imageView.image
                        
                        //self.updateImageConstraints()
                        
                    }
                    
                    
                }
                
            }
            
        }
        
        
    }
    
    func presentCropper(){
        
        cropViewController = CropViewController(image: imageView.image!)
        cropViewController.delegate = self
        
        
        present(cropViewController, animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true) {
            
            self.imageView.image = image
            
        }
        
    }
    
    var clippingScoresAbove: Double { return 0.7 }
    
    /// Values lower than this value will not appear in the mask.
    var zeroingScoresBelow: Double { return 0.3 }
    
    /// Controls the opacity the mask is applied to the base image.
    var opacity: CGFloat!
    
    /// The method used to blend the hair mask with the underlying image.
    /// Soft light produces the best results in our tests, but check out
    /// .hue and .color for different effects.
    var blendMode: CIBlendKernel { return .softLight }
    
    /// Color of the mask.
    var maskColor = UIColor.red
    
    var blended: UIImage!
    
    var hairColorChanged = false
    
    var imageForFritz: FritzVisionImage!
    
    var imageFromMainThread: UIImage!
    
    func changeHairColor(){
        
        if self.hairColorChanged == false {
            
            self.imageForFritz = FritzVisionImage(image: self.imageFromMainThread)
            
            self.hairColorChanged = true
        }
        
        self.opacity = CGFloat(self.balloonPickerView.value) / CGFloat(100)
        
        
        
        self.imageForFritz.metadata?.orientation = .up
        
        
        guard let result = try? self.visionModel.predict(self.imageForFritz),
            let mask = result.buildSingleClassMask(
                forClass: FritzVisionHairClass.hair,
                clippingScoresAbove: self.clippingScoresAbove,
                zeroingScoresBelow: self.zeroingScoresBelow,
                resize: false,
                color: self.maskColor)
            else { return }
        
        self.blended = self.imageForFritz.blend(
            withMask: mask,
            blendKernel: self.blendMode,
            opacity: self.opacity
        )
        
        
        
    }
    
    @objc func cancelTransoformF(){
        
        if let number = UserDefaults.standard.object(forKey: "firstOpenNumber") as? Int {
            
            UserDefaults.standard.set(number, forKey: "currentImageNumber")
            
            if let data = UserDefaults.standard.object(forKey: "firstOpen") as? NSData {
                
                UserDefaults.standard.set(data, forKey: "image\(number)")
                
            }
            
        }
        
        dismiss(animated: true, completion: nil)
        
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Image saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    let imagePickerController = UIImagePickerController()
    
    @objc func saveCurrentImageF(){
        
        //save current state image
        
        dismiss(animated: true, completion: nil)
        
        /*imagePickerController.delegate = self
         imagePickerController.allowsEditing = false
         
         imagePickerController.sourceType = .photoLibrary
         present(imagePickerController, animated: true, completion: nil)*/
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            
            return self.imagePickerControllerDidCancel(picker)
            
        }
        
        self.imageView.image = image
        //peopleApi()
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        picker.delegate = nil
    }
    
    var longPressed = false
    
    var currentImageDisplayed: UIImage!
    
    @objc func handleLongPress(){
        
        
        
        if longPressed == false {
            
            currentImageDisplayed = imageView.image
            
            if let originalImage = UserDefaults.standard.object(forKey: "image1") as? NSData {
                
                self.imageView.image = UIImage(data: originalImage as Data)
                
               // self.updateImageConstraints()
                
            }
            
            longPressed = true
            
            
        }else {
            
            imageView.image = currentImageDisplayed
            
           // self.updateImageConstraints()
            
            longPressed = false
            
        }
        
    }
    
    var onSmoother = true
    var onLighten = false
    var onMask = false
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = menuCV.dequeueReusableCell(withReuseIdentifier: "MenuId", for: indexPath) as! MenuCell
        
        cell.modeImage.image = UIImage(named: modeImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        //cell.modeImage.tintColor = .black
        //cell.modeTitle.textColor = .black
        
        
        cell.modeTitle.text = modeTitles[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            maskImageView.alpha = 0
            lightenImageView.alpha = 0
            
            sliderLbl.text = "Smoother"
            
            topConstraint.isActive = false
            topConstraint.constant = -2
            topConstraint.isActive = true
            balloonChanged()
            balloonPickerView.alpha = 1
            removeColorSlider()
            fadeOutTimes()
            
            onSmoother = true
            onLighten = false
            onMask = false
            
            UIView.animate(withDuration: 0.7) {
                
                self.view.layoutIfNeeded()
                
            }
            
            startTouch = false
            shouldChangeHairColor = false
            
            //processImage()
            
        }else if indexPath.row == 1 {
            
            imageFromMainThread = imageView.image!
            
            maskImageView.alpha = 0
            lightenImageView.alpha = 0
            
            shouldChangeHairColor = true
            //fadeOutSlider()
            //fadeInTimes()
            sliderLbl.text = "Hair Color"
            
            onSmoother = false
            onLighten = false
            onMask = false
            
            balloonChanged()
            removeBottomSlider()
            
            
            rightConstraint.isActive = false
            rightConstraint.constant = -35
            rightConstraint.isActive = true
            
            UIView.animate(withDuration: 0.7) {
                
                self.view.layoutIfNeeded()
                
            }
            
            startTouch = false
           // changeHairColor()
            
        }else if indexPath.row == 2 {
            
            maskImageView.alpha = 0
            lightenImageView.alpha = 1
            shouldChangeHairColor = false
            
            sliderLbl.text = "Lighten"
            
            removeColorSlider()
            fadeOutTimes()
            
            topConstraint.isActive = false
            topConstraint.constant = -2
            topConstraint.isActive = true
            
            
            UIView.animate(withDuration: 0.7) {
                
                self.view.layoutIfNeeded()
                
            }
            
            onSmoother = false
            onLighten = true
            onMask = false
            
            startTouch = true
            
        }else if indexPath.row == 3 {
            
            maskImageView.alpha = 1
            lightenImageView.alpha = 0
            blurImage()
            shouldChangeHairColor = false
            
            sliderLbl.text = "Mask"
            
            fadeOutTimes()
            
            removeColorSlider()
            
            onSmoother = false
            onLighten = false
            onMask = true
            
            startTouch = false
            
        }else if indexPath.row == 4 {
            
            maskImageView.alpha = 0
            lightenImageView.alpha = 0
            shouldChangeHairColor = false
            
            fadeOutTimes()
            
            removeColorSlider()
            
            onSmoother = false
            onLighten = false
            onMask = false
            
            startTouch = false
            
            callAlert()
            
        }
        
    }
    
    func callAlert(){
        
        let alert = UIAlertController(title: "Original Image", message: "Remove all editing to original image ? This action is not reversable.", preferredStyle: .alert)
        
        let okay = UIAlertAction(title: "Okay", style: .default) { (action) in
            
            
            
            //
            
            if let imageNumber = UserDefaults.standard.object(forKey: "firstOpenNumber") as? Int {
                
                
                UserDefaults.standard.set(imageNumber, forKey: "currentImageNumber")
                
                if let imageData = UserDefaults.standard.object(forKey: "firstOpen") as? NSData {
                    
                    self.imageView.image = UIImage(data: imageData as Data)
                    UserDefaults.standard.set(imageData, forKey: "image\(imageNumber)")
                    self.imageToSmooth = self.imageView.image
                    
                }
                
            }
            
            //self.updateImageConstraints()
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        alert.addAction(okay)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func blurImage(){
        
        loadingLoaderBackground.alpha = 1
        loadingLoader.startAnimating()
        loaderBackground.alpha = 0.7
        view.isUserInteractionEnabled = false
        
        let originalImageExtent = CIImage(image: self.firstMaskImage)?.extent
        
        let ciImg = CIImage(image: self.firstMaskImage)
        
        DispatchQueue.global(qos: .default).async {
            
            // Do heavy work here
            //self.changeHairColor()
            
            let blur = CIFilter(name: "CIGaussianBlur")
            blur?.setValue(ciImg, forKey: kCIInputImageKey)
            blur?.setValue(10.0, forKey: kCIInputRadiusKey)
            let outputImg = blur?.outputImage
            
            
            DispatchQueue.main.async { [weak self] in
                // UI updates must be on main thread
               
                self!.maskImageView.image = UIImage(cgImage: CIContext(options:nil).createCGImage(outputImg!, from: originalImageExtent!)!)
                
                
                self!.loadingLoaderBackground.alpha = 0
                self!.loadingLoader.stopAnimating()
                self!.loaderBackground.alpha = 0
                self!.view.isUserInteractionEnabled = true
                
                
            }
        }
        
    }
    
    func fadeOutSlider(){
        
        UIView.animate(withDuration: 0.7) {
            
            self.balloonPickerView.alpha = 0
            
        }
    }
    
    func fadeInTimes(){
        
        
        
        UIView.animate(withDuration: 0.7) {
            
            self.timesHolder.alpha = 1
            
        }
        
    }
    
    func fadeOutTimes(){
        
        UIView.animate(withDuration: 0.7) {
            
            self.timesHolder.alpha = 0
            
        }
        
    }
    
    func removeBottomSlider(){
        
        
        topConstraint.isActive = false
        topConstraint.constant = 105
        topConstraint.isActive = true
        
        UIView.animate(withDuration: 0.7) {
            
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    func removeColorSlider(){
        
        rightConstraint.isActive = false
        rightConstraint.constant = 75
        rightConstraint.isActive = true
        
        UIView.animate(withDuration: 0.7) {
            
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    @objc func callTransform(){
        
        let vC = TransformViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callBeautify(){
        
        let vC = BeautifyViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    @objc func callMask(){
        
        let vC = MaskViewController()
        
        present(vC, animated: true, completion: nil)
        
    }
    
    @objc func callFilter(){
        
        let vC = FilterViewController()
        
        present(vC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: view.frame.width / 5, height: view.frame.width / 5)
        
        return size
        
    }
    
    var currentPoint: CGPoint!
    var lastPoint: CGPoint!
    
    var startTouch = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if startTouch == true || onMask == true {
            
            for touch in touches {
                
                lastPoint = touch.location(in: imageView)
                
            }
            
        }
        
    }
    
    var didTouch = false
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if startTouch == true {
            
            for touch in touches {
                
                currentPoint = touch.location(in: lightenImageView)
                
                didTouch = true
                
            }
            
            
            //UIGraphicsBeginImageContext(imageView.frame.size)
            
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(lightenImageView.frame.size, !hasAlpha, scale)
            
            lightenImageView.image!.draw(in: CGRect(x: 0, y: 0, width: lightenImageView.frame.size.width, height: lightenImageView.frame.size.height))
            
            print(imageView.frame.size)
            
            
            UIGraphicsGetCurrentContext()?.saveGState()
            UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
            UIGraphicsGetCurrentContext()!.setLineCap(CGLineCap.round)
            UIGraphicsGetCurrentContext()?.setLineWidth(CGFloat(lineWidth))
            UIGraphicsGetCurrentContext()?.setShadow(offset: CGSize(width: 0, height: 0), blur: 25, color: UIColor.white.cgColor)
            
            
            
            let path = CGMutablePath()
            path.move(to: CGPoint(x: lastPoint.x, y: lastPoint.y), transform: .identity)
            path.addLine(to: CGPoint(x: currentPoint.x, y: currentPoint.y), transform: .identity)
            UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.clear)
            
            UIGraphicsGetCurrentContext()?.addPath(path)
            UIGraphicsGetCurrentContext()?.strokePath()
            
            lightenImageView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsGetCurrentContext()?.restoreGState()
            UIGraphicsEndImageContext()
            
            lastPoint = currentPoint
            
        }else if onMask == true {
            
            for touch in touches {
                
                currentPoint = touch.location(in: imageView)
                
                didTouch = true
                
            }
            
            //UIGraphicsBeginImageContext(imageView.frame.size)
            
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(imageView.frame.size, !hasAlpha, scale)
            
            imageView.image!.draw(in: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
            
            print(imageView.frame.size)
            
            
            UIGraphicsGetCurrentContext()?.saveGState()
            UIGraphicsGetCurrentContext()?.setShouldAntialias(true)
            UIGraphicsGetCurrentContext()!.setLineCap(CGLineCap.round)
            UIGraphicsGetCurrentContext()?.setLineWidth(CGFloat(lineWidth))
            
            let path = CGMutablePath()
            path.move(to: CGPoint(x: lastPoint.x, y: lastPoint.y), transform: .identity)
            path.addLine(to: CGPoint(x: currentPoint.x, y: currentPoint.y), transform: .identity)
            UIGraphicsGetCurrentContext()!.setBlendMode(CGBlendMode.clear)
            
            UIGraphicsGetCurrentContext()?.addPath(path)
            UIGraphicsGetCurrentContext()?.strokePath()
            
            imageView.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsGetCurrentContext()?.restoreGState()
            UIGraphicsEndImageContext()
            
            lastPoint = currentPoint
            
            
        }
        
       
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if didTouch == true && startTouch == true {
            
            let bottomImage = lightenImageView.image
            let topImage = imageView.image
            
            let size = imageView.frame.size
            UIGraphicsBeginImageContext(size)
            
            let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            bottomImage!.draw(in: areaSize)
            
            topImage!.draw(in: areaSize, blendMode: .color, alpha: 0.8)
            
            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imageView.image = newImage
            
            imageToSmooth = newImage
            
            saveCurrentImageStateF()
            
            didTouch = false
            
        }else if didTouch == true && onMask == true {
            
            let bottomImage = maskImageView.image
            let topImage = imageView.image
            
            let size = imageView.frame.size
            UIGraphicsBeginImageContext(size)
            
            let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            bottomImage!.draw(in: areaSize)
            
            topImage!.draw(in: areaSize, blendMode: .normal, alpha: 0.95)
            
            let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            imageView.image = newImage
            
            imageToSmooth = newImage
            
            saveCurrentImageStateF()
            
            didTouch = false
            
            
        }
        
    }
    
    
}
